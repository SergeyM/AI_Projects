"""SegmentationNN"""
import torch
import torch.nn as nn
import torchvision.models as models

class SegmentationNN(nn.Module):

    def __init__(self, num_classes=23):
        super(SegmentationNN, self).__init__()

        ########################################################################
        #                             YOUR CODE                                #
        ########################################################################

        vgg = models.vgg16(pretrained=True)
        vgg = nn.Sequential(*list(vgg.children())[:-1])
        for param in vgg.parameters():
            param.requires_grad = False

        self.vgg = vgg
        self.fcc_1 = nn.Conv2d(512, 4096, (3, 3))
        self.fcc_2 = nn.Conv2d(4096, 4096, (1, 1))
        self.fcc_3 = nn.Conv2d(4096, 24, (1, 1))
        self.upsample = nn.Upsample(size=(240, 240))


        """
        feats = list(models.vgg16(pretrained=True).features.children())
        self.feats = nn.Sequential(*feats[0:9])
        self.feat3 = nn.Sequential(*feats[10:16])
        self.feat4 = nn.Sequential(*feats[17:23])
        self.feat5 = nn.Sequential(*feats[24:30])

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                m.requires_grad = False

        self.fconn = nn.Sequential(
            nn.Conv2d(512, 4096, 7),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Conv2d(4096, 4096, 1),
            nn.ReLU(inplace=True),
            nn.Dropout(),
        )
        self.score_feat3 = nn.Conv2d(256, num_classes, 1)
        self.score_feat4 = nn.Conv2d(512, num_classes, 1)
        self.score_fconn = nn.Conv2d(4096, num_classes, 1)

        self.upsample1 = nn.Upsample(size=512, mode='bilinear', align_corners=True)
        self.upsample2 = nn.Upsample(size=256, mode='bilinear', align_corners=True)
        self.upsample3 = nn.Upsample(size=(240, 240), mode='bilinear', align_corners=True)
        """
        ########################################################################
        #                             END OF YOUR CODE                         #
        ########################################################################

    def forward(self, x):
        """
        Forward pass of the convolutional neural network. Should not be called
        manually but by calling a model instance directly.

        Inputs:
        - x: PyTorch input Variable
        """
        ########################################################################
        #                             YOUR CODE                                #
        ########################################################################

        x = self.vgg(x)
        x = self.fcc_1(x)
        x = self.fcc_2(x)
        x = self.fcc_3(x)
        x = self.upsample(x)

        """
        feats = self.feats(x)
        feat3 = self.feat3(feats)
        feat4 = self.feat4(feat3)
        feat5 = self.feat5(feat4)
        fconn = self.fconn(feat5)

        score_feat3 = self.score_feat3(feat3)
        score_feat4 = self.score_feat4(feat4)
        score_fconn = self.score_fconn(fconn)

        print(score_feat4.size())
        score = self.upsample1(score_fconn)#F.upsample_bilinear(score_fconn, score_feat4.size()[2:])
        score += score_feat4
        print(score_feat3.size())
        score = self.upsample2(score)#F.upsample_bilinear(score, score_feat3.size()[2:])
        score += score_feat3
        print(x.size())
        score = self.upsample3(score) #F.upsample_bilinear(score, x.size()[2:])
        """

        ########################################################################
        #                             END OF YOUR CODE                         #
        ########################################################################

        return x

    @property
    def is_cuda(self):
        """
        Check if model parameters are allocated on the GPU.
        """
        return next(self.parameters()).is_cuda

    def save(self, path):
        """
        Save model with its parameters to the given path. Conventionally the
        path should end with "*.model".

        Inputs:
        - path: path string
        """
        print('Saving model... %s' % path)
        torch.save(self, path)
