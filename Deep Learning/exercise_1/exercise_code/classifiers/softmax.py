"""Linear Softmax Classifier."""
# pylint: disable=invalid-name
import numpy as np

from itertools import product
from .linear_classifier import LinearClassifier


def cross_entropoy_loss_naive(W, X, y, reg):
    """
    Cross-entropy loss function, naive implementation (with loops)

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
      that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    # pylint: disable=too-many-locals
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    ############################################################################
    # TODO: Compute the cross-entropy loss and its gradient using explicit     #
    # loops. Store the loss in loss and the gradient in dW. If you are not     #
    # careful here, it is easy to run into numeric instability. Don't forget   #
    # the regularization!                                                      #
    ############################################################################

    (N, ) = y.shape
    (D, C) = W.shape
    output = np.zeros((N, C))

    for i in range(N):
        # add up all inputs with weights
        output[i] = X[i].dot(W)

        # Calculate the activation (Softmax Activation Function)
        output[i] = np.exp(output[i])/np.sum(np.exp(output[i]))

        # Calculate Cross Entropy Loss Function
        loss += -1*np.log(output[i][y[i]])

        # Calculate gradient
        for j in range(D):
            for k in range(C):
                dW[j][k] += (output[i][k] - (1 if k == y[i] else 0)) * X[i][j]

    # Calculate average Loss from all samples
    loss = loss/N + (0.5 * reg * np.sum(W * W))

    # Calculate gradient average
    dW = dW/N + (reg * W)

    ############################################################################
    #                          END OF YOUR CODE                                #
    ############################################################################

    return loss, dW


def cross_entropoy_loss_vectorized(W, X, y, reg):
    """
    Cross-entropy loss function, vectorized version.

    Inputs and outputs are the same as in cross_entropoy_loss_naive.
    """
    # Initialize the loss and gradient to zero.
    loss = 0.0
    dW = np.zeros_like(W)

    ############################################################################
    # TODO: Compute the cross-entropy loss and its gradient without explicit   #
    # loops. Store the loss in loss and the gradient in dW. If you are not     #
    # careful here, it is easy to run into numeric instability. Don't forget   #
    # the regularization!                                                      #
    ############################################################################

    (N, ) = y.shape
    (D, C) = W.shape

    # matrix multiplication (output = X * W)
    output = np.matmul(X, W)

    # Calculate the activation (Softmax Activation Function)
    # For numerical stability:
    output = output - np.expand_dims(np.max(output, axis=1), 1)
    output = np.exp(output)
    ax_sum = np.expand_dims(np.sum(output, axis=1), 1)
    output = output / ax_sum

    # Calculation Loss
    lossVector = output[np.arange(N), y]
    loss = (np.sum(-1*np.log(lossVector)) / N) + (0.5 * reg * np.sum(W * W))

    # Calculation gradient
    output[np.arange(N), y] -= 1
    dW = (np.matmul(np.transpose(X), output) / N) + (reg * W)

    ############################################################################
    #                          END OF YOUR CODE                                #
    ############################################################################

    return loss, dW


class SoftmaxClassifier(LinearClassifier):
    """The softmax classifier which uses the cross-entropy loss."""

    def loss(self, X_batch, y_batch, reg):
        return cross_entropoy_loss_vectorized(self.W, X_batch, y_batch, reg)


def softmax_hyperparameter_tuning(X_train, y_train, X_val, y_val):
    # results is dictionary mapping tuples of the form
    # (learning_rate, regularization_strength) to tuples of the form
    # (training_accuracy, validation_accuracy). The accuracy is simply the
    # fraction of data points that are correctly classified.
    results = {}
    best_val = -1
    best_softmax = None
    all_classifiers = []
    learning_rates = [1e-7, 7e-7]
    regularization_strengths = [1e4, 4e4]

    ############################################################################
    # TODO:                                                                    #
    # Write code that chooses the best hyperparameters by tuning on the        #
    # validation set. For each combination of hyperparameters, train a         #
    # classifier on the training set, compute its accuracy on the training and #
    # validation sets, and  store these numbers in the results dictionary.     #
    # In addition, store the best validation accuracy in best_val and the      #
    # Softmax object that achieves this accuracy in best_softmax.              #                                      #
    #                                                                          #
    # Hint: You should use a small value for num_iters as you develop your     #
    # validation code so that the classifiers don't take much time to train;   #
    # once you are confident that your validation code works, you should rerun #
    # the validation code with a larger value for num_iters.                   #
    ############################################################################

    # best solution 1e-7, 1e4
    learning_rate_steps = 0.3e-7
    regularization_strength_steps = 0.5e4

    for rate, reg in product(np.arange(learning_rates[0], learning_rates[1]+learning_rate_steps, learning_rate_steps), np.arange(regularization_strengths[0], regularization_strengths[1]+regularization_strength_steps, regularization_strength_steps)):
        softmax = SoftmaxClassifier()
        softmax.train(X_train, y_train, learning_rate=rate, reg=reg, num_iters=1200, verbose=False)
        y_train_pred = softmax.predict(X_train)
        train_accuracy_temp = np.mean(y_train == y_train_pred)
        y_val_pred = softmax.predict(X_val)
        val_accuracy_temp = np.mean(y_val == y_val_pred)
        results[(rate, reg)] = (train_accuracy_temp, val_accuracy_temp)
        all_classifiers.append(softmax)
        if val_accuracy_temp > best_val:
            best_val = val_accuracy_temp
            best_softmax = softmax
        print('finish %e %e' % (rate, reg))

    ############################################################################
    #                              END OF YOUR CODE                            #
    ############################################################################

    # Print out results.
    for (lr, reg) in sorted(results):
        train_accuracy, val_accuracy = results[(lr, reg)]
        print('lr %e reg %e train accuracy: %f val accuracy: %f' % (
              lr, reg, train_accuracy, val_accuracy))

    print('best validation accuracy achieved during validation: %f' % best_val)

    return best_softmax, results, all_classifiers
