from _nsis import out
from random import shuffle
import numpy as np

import torch
from torch.autograd import Variable


class Solver(object):
    default_adam_args = {"lr": 1e-4,
                         "betas": (0.9, 0.999),
                         "eps": 1e-8,
                         "weight_decay": 0.0}

    def __init__(self, optim=torch.optim.Adam, optim_args={},
                 loss_func=torch.nn.CrossEntropyLoss()):
        optim_args_merged = self.default_adam_args.copy()
        optim_args_merged.update(optim_args)
        self.optim_args = optim_args_merged
        self.optim = optim
        self.loss_func = loss_func

        self._reset_histories()

    def _reset_histories(self):
        """
        Resets train and val histories for the accuracy and the loss.
        """
        self.train_loss_history = []
        self.train_acc_history = []
        self.val_acc_history = []
        self.val_loss_history = []

    def train(self, model, train_loader, val_loader, num_epochs=10, log_nth=0):
        """
        Train a given model with the provided data.

        Inputs:
        - model: model object initialized from a torch.nn.Module
        - train_loader: train data in torch.utils.data.DataLoader
        - val_loader: val data in torch.utils.data.DataLoader
        - num_epochs: total number of training epochs
        - log_nth: log training accuracy and loss every nth iteration
        """
        optim = self.optim(model.parameters(), **self.optim_args)
        self._reset_histories()
        iter_per_epoch = len(train_loader)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        model.to(device)

        print('START TRAIN.')
        ########################################################################
        # TODO:                                                                #
        # Write your own personal training method for our solver. In each      #
        # epoch iter_per_epoch shuffled training batches are processed. The    #
        # loss for each batch is stored in self.train_loss_history. Every      #
        # log_nth iteration the loss is logged. After one epoch the training   #
        # accuracy of the last mini batch is logged and stored in              #
        # self.train_acc_history. We validate at the end of each epoch, log    #
        # the result and store the accuracy of the entire validation set in    #
        # self.val_acc_history.                                                #
        #                                                                      #
        # Your logging could like something like:                              #
        #   ...                                                                #
        #   [Iteration 700/4800] TRAIN loss: 1.452                             #
        #   [Iteration 800/4800] TRAIN loss: 1.409                             #
        #   [Iteration 900/4800] TRAIN loss: 1.374                             #
        #   [Epoch 1/5] TRAIN acc/loss: 0.560/1.374                            #
        #   [Epoch 1/5] VAL   acc/loss: 0.539/1.310                            #
        #   ...                                                                #
        ########################################################################

        num_iterations = iter_per_epoch * num_epochs

        for epoch in range(num_epochs):
            for i, (inputs, labels) in enumerate(train_loader):
                inputs, labels = Variable(inputs), Variable(labels)

                optim.zero_grad()

                output = model(inputs)
                loss = self.loss_func(output, labels.type(torch.LongTensor))
                self.train_loss_history.append(loss)

                if i % log_nth == 0:
                    print('[Iteration ' + str(iter_per_epoch*epoch+i+1) + '/' + str(num_iterations) + '] TRAIN loss: ' + str(loss.data.numpy()))

                loss.backward()
                optim.step()

            (train_X, train_y) = next(iter(train_loader))
            train_out = model(train_X)
            train_loss = self.loss_func(train_out, train_y.type(torch.LongTensor))
            self.train_acc_history.append((train_out.max(dim = 1)[1] == train_y.type(torch.LongTensor)).sum().data.numpy() / train_y.shape[0])
            print('[Epoch ' + str(epoch+1) + '/' + str(num_epochs) + '] TRAIN acc/loss: ' + str(self.train_acc_history[-1]) + '/' + str(train_loss.data.numpy()))

            scores = []
            val_loss = []
            for val_X, val_y in val_loader:
                val_X, val_y = val_X.to(device), val_y.to(device)
                val_X, val_y = Variable(val_X), Variable(val_y)
                val_out = model(val_X)
                val_loss.append(self.loss_func(val_out, val_y.type(torch.LongTensor)).data.numpy())
                scores.extend((val_out.max(dim = 1)[1] == val_y.type(torch.LongTensor)).data.cpu().numpy())

            self.val_acc_history.append(np.mean(scores))
            print('[Epoch ' + str(epoch+1) + '/' + str(num_epochs) + '] VAL   acc/loss: ' + str(self.val_acc_history[-1]) + '/' + str(np.mean(val_loss)))

        ########################################################################
        #                             END OF YOUR CODE                         #
        ########################################################################
        print('FINISH.')
