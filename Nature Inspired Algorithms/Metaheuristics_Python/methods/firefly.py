import numpy as np
from methods.method import Method


class Firefly(Method):
    def __init__(self, problem, num, alpha=1, beta=1, gamma=0, std=0.1, damping=0.99):
        Method.__init__(self, problem, num)

        # Randomization parameter
        if problem.__class__.__name__ == 'TSP':
            self.alpha = int(int(self.problem.header['DIMENSION']) * min(alpha, 1))
        elif problem.__class__.__name__ == 'FunctionOptimization':
            self.alpha = alpha

        self.damping = damping

        self.beta = beta            # Attractiveness value

        # Light absorption
        if gamma == 0:
            if problem.__class__.__name__ == 'TSP':
                self.gamma = 0.1
            elif problem.__class__.__name__ == 'FunctionOptimization':
                self.gamma = 1
        else:
            self.gamma = gamma

        # Gaussian distribution parameter
        self.std = std

        # sort both arrays, so best agents is first (in this case lowest light_intensity)
        self.sort_agents()

    def calculate_distance(self, pos1, pos2):
        # number of different arcs
        """
        dimension = int(self.problem.header['DIMENSION'])
        count = 0
        for i in range(dimension):
            edge = (pos1[i], pos1[(i+1) % dimension])
            count += 1 if pos2[(np.where(pos2 == edge[0])[0] + 1) % dimension] != edge[1] else 0
        return count
        """

        # swap distance
        """
        dimension = int(self.problem.header['DIMENSION'])
        pos2_copy = np.copy(pos2)
        count = 0
        for i in range(dimension):
            edge = (pos1[i], pos1[(i+1) % dimension])
            index = (np.where(pos2_copy == edge[0])[0] + 1) % dimension
            if pos2_copy[index] != edge[1]:
                index2 = np.where(pos2_copy == edge[1])[0]
                pos2_copy[index], pos2_copy[index2] = pos2_copy[index2], pos2_copy[index]
                count += 1
        return count
        """

        # Hamming distance
        return np.count_nonzero(pos1 != pos2)

    def move_firefly(self, pos_from, pos_to):
        dimension = int(self.problem.header['DIMENSION'])

        # beta step
        probability = self.beta / (1 + self.gamma * self.calculate_distance(pos_from, pos_to))

        pos_from_copy = np.copy(pos_from) + 1
        pos_to_copy = np.copy(pos_to) + 1

        new_pos = np.zeros(int(self.problem.header['DIMENSION']), dtype=int)
        index = np.where(pos_from == pos_to)[0]
        new_pos[index] = pos_from_copy[index]

        for i in range(dimension):
            if new_pos[i] == 0:
                random = np.random.rand()
                if random < probability:
                    if pos_to_copy[i] not in new_pos:
                        new_pos[i] = pos_to_copy[i]
                else:
                    if pos_from_copy[i] not in new_pos:
                        new_pos[i] = pos_from_copy[i]

        unused = np.setdiff1d(pos_from_copy, new_pos)
        np.random.shuffle(unused)
        new_pos[new_pos == 0] = unused

        # alpha step
        if int(self.alpha) > 0:
            new_pos = self.move_random(new_pos, int(self.alpha))

        return new_pos-1

    def move_random(self, pos, alpha):
        new_pos = np.copy(pos)
        shuffle_index = np.random.choice(new_pos.shape[0], np.random.randint(alpha) + 1, replace=False)
        shuffle_values = new_pos[shuffle_index]
        np.random.shuffle(shuffle_index)
        new_pos[shuffle_index] = shuffle_values
        return new_pos


    def calculateNextGen_continuous(self):
        # Approach 3
        # self.agents[0] += np.random.normal(0, self.std, self.problem.dimension)

        for i in range(self.num):
            for j in range(self.num):
                # Uniform random
                random = np.random.uniform(size=self.problem.dimension)
                randomization = self.alpha * (random - 0.5)

                # Normal random
                # random = np.random.normal(0, self.std, self.problem.dimension)
                # randomization = self.alpha * random

                if self.fitness[i] > self.fitness[j]:
                    distance = np.linalg.norm(self.agents[j] - self.agents[i])
                    # Approach 1
                    attractiveness = self.beta * np.exp(-self.gamma*distance**2)
                    # Approach 2
                    # attractiveness = self.beta / (1 + self.gamma * distance**2)

                    delta = attractiveness * (self.agents[j] - self.agents[i]) + randomization
                    self.agents[i] += delta
                # else:
                # Approach 1
                # self.agents[i] += randomization
                # Approach 2
                # self.agents[i] += random

                self.agents[i] = np.clip(self.agents[i], self.problem.lowerBound, self.problem.upperBound)
                self.fitness[i] = self.problem.function(self.agents[i])

    def calculateNextGen_discrete(self):
        # Best moves randomly
        # self.agents[0] = self.move_random(self.agents[0], 2)

        for i in range(self.num):
            for j in range(self.num):
                if self.fitness[i] > self.fitness[j]:
                    self.agents[i] = self.move_firefly(self.agents[i], self.agents[j])
                # else:
                #   Move randomly????????

                self.fitness[i] = self.problem.calculate_fitness(np.array([self.agents[i]]))


    def calculateNextGen(self):
        self.alpha = self.alpha * self.damping

        if self.problem.__class__.__name__ == 'TSP':
            self.calculateNextGen_discrete()
        elif self.problem.__class__.__name__ == 'FunctionOptimization':
            self.calculateNextGen_continuous()

        self.sort_agents()
        self.save_state()



