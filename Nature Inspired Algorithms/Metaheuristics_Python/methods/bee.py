import numpy as np
from methods.method import Method

# TODO TSP with Bee

class Bee(Method):
    def __init__(self, problem, num, e_bees=7, o_bees=2, num_sites=3, elite_sites=1, patch_size=3.0, damp_amount=0.95):
        Method.__init__(self, problem, num)

        self.e_bees = e_bees
        self.o_bees = o_bees
        self.num_sites = num_sites
        self.elite_sites = elite_sites
        self.patch_size = patch_size
        self.damping = damp_amount

        # sort both arrays, so best agents is first (in this case lowest light_intensity)
        self.sort_agents()

    def calculateNextGen(self):
        top_sites = self.agents[0:self.num_sites]
        new_generation = np.zeros(self.agents.shape)
        for i in range(self.num_sites):
            new_generation[i] = self.searchNeighborhood(top_sites[i], self.e_bees if i < self.elite_sites else self.o_bees)
        new_generation[self.num_sites:] = np.random.uniform(self.problem.lowerBound, self.problem.upperBound, (self.num - self.num_sites, self.problem.dimension))

        self.patch_size = self.patch_size * self.damping
        self.agents = new_generation
        self.agents = np.clip(self.agents, self.problem.lowerBound, self.problem.upperBound)
        self.fitness = self.problem.calculate_fitness(self.agents)

        self.sort_agents()
        self.save_state()

    def searchNeighborhood(self, site, num_bees):
        # TODO implement neighborhood search
        return 5
