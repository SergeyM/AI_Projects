from abc import ABC, abstractmethod
import numpy as np

class Method(ABC):
    def __init__(self, problem, num):
        self.problem = problem
        self.num = num

        self.agents = problem.generate_population(self.num)
        self.fitness = self.problem.calculate_fitness(self.agents)

        self.bestSolution = self.agents[0]
        self.bestFitness = float('Inf')
        self.best = np.array([])
        self.average = np.array([])
        self.worst = np.array([])

    @abstractmethod
    def calculateNextGen(self):
        pass

    def sort_agents(self):
        indices = self.fitness.argsort()
        self.fitness = np.array(self.fitness)[indices]
        self.agents = np.array(self.agents)[indices]

    def save_state(self):
        if self.fitness.min() < self.bestFitness:
            self.bestFitness = self.fitness.min()
            self.bestSolution = self.agents[self.fitness.argmin()]

        self.best = np.append(self.best, [self.fitness.min()])
        self.average = np.append(self.average, [np.mean(self.fitness)])
        self.worst = np.append(self.worst, [self.fitness.max()])

