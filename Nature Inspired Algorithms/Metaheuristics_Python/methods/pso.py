import numpy as np
from methods.method import Method


class PSO(Method):
    def __init__(self, problem, num, c1=2, c2=2, w=1, damp_amount=0.99):
        Method.__init__(self, problem, num)

        self.c1 = c1
        self.c2 = c2
        self.w = w
        self.damping = damp_amount

        if problem.__class__.__name__ == 'TSP':
            self.currentVelocity = np.zeros((self.num, int(self.problem.header['DIMENSION']), int(self.problem.header['DIMENSION'])))
        elif problem.__class__.__name__ == 'FunctionOptimization':
            self.currentVelocity = np.zeros((self.problem.dimension, num))

        self.personalBestPosition = np.copy(self.agents)
        self.personalBest = self.fitness
        self.globalBestPosition = self.personalBestPosition[self.personalBest.argmin()]
        self.globalBest = self.personalBest.min()

    def calculate_velocity(self, c, pos1, pos2):
        dimension = int(self.problem.header['DIMENSION'])
        velocity = np.zeros((dimension, dimension))
        for i in range(dimension):
            edge = (pos1[i], pos1[(i+1) % dimension])
            in_pos2 = pos2[(np.where(pos2 == edge[0])[0] + 1) % dimension] == edge[1] or pos2[(np.where(pos2 == edge[0])[0] - 1) % dimension] == edge[1]
            if not in_pos2:
                velocity[edge] = c if c <= 1 else 1
                velocity[edge[::-1]] = c if c <= 1 else 1
        return velocity

    def position_update(self, pos, vel):
        alpha = np.random.rand()
        velocity = np.copy(vel)
        velocity[np.where(velocity < alpha)] = 0

        old_pos = np.copy(pos) + 1

        dimension = int(self.problem.header['DIMENSION'])
        new_pos = np.zeros(dimension)
        new_pos[0] = np.random.randint(dimension) + 1

        for i in range(dimension-1):
            choice = 0
            valid = False

            temp = np.where(velocity[int(new_pos[i] - 1)] > 0)[0]
            if temp.size != 0:
                # choice_list = np.random.permutation(temp) + 1
                choice_list = temp[np.argsort(self.problem.distances[int(new_pos[i] - 1)][temp])] + 1
                for candidate in range(choice_list.shape[0]):
                    if choice_list[candidate] not in new_pos:
                        choice = choice_list[candidate]
                        valid = True
                        break

            if not valid:
                index = np.where(old_pos == new_pos[i])[0][0]
                choice_list = old_pos[[(index-1) % dimension, (index+1) % dimension]]
                # choice_list = np.random.permutation(choice_list)
                choice_list = choice_list[np.argsort(self.problem.distances[int(new_pos[i] - 1)][choice_list - 1])]
                for candidate in range(choice_list.shape[0]):
                    if choice_list[candidate] not in new_pos:
                        choice = choice_list[candidate]
                        valid = True
                        break

            if not valid:
                choice_list = np.argsort(self.problem.distances[int(new_pos[i] - 1)]) + 1
                for candidate in range(1, choice_list.shape[0]):
                    if choice_list[candidate] not in new_pos:
                        choice = choice_list[candidate]
                        break
            new_pos[i+1] = choice

        return new_pos - 1

    def calculateNextGen_continuous(self):
        r1 = np.random.random(self.num)
        r2 = np.random.random(self.num)

        deltaPBest = self.c1 * np.multiply((self.personalBestPosition.transpose() - self.agents.transpose()), r1)
        deltaGBest = self.c2 * np.multiply((self.globalBestPosition - self.agents).transpose(), r2)
        self.currentVelocity = (self.w * self.currentVelocity) + deltaPBest + deltaGBest
        self.agents += self.currentVelocity.transpose()
        self.agents = np.clip(self.agents, self.problem.lowerBound, self.problem.upperBound)

    def calculateNextGen_discrete(self):
        r1 = np.random.random(self.num)
        r2 = np.random.random(self.num)

        p_best_velocities = np.zeros(self.currentVelocity.shape)
        g_best_velocities = np.zeros(self.currentVelocity.shape)
        for i in range(p_best_velocities.shape[0]):
            p_best_velocities[i] = self.calculate_velocity(r1[i]*self.c1, self.personalBestPosition[i], self.agents[i])
            g_best_velocities[i] = self.calculate_velocity(r2[i]*self.c2, self.globalBestPosition, self.agents[i])
        self.currentVelocity = np.maximum(np.maximum(self.w * self.currentVelocity, p_best_velocities), g_best_velocities)

        for i in range(self.agents.shape[0]):
            self.agents[i] = self.position_update(self.agents[i], self.currentVelocity[i])

    def calculateNextGen(self):
        if self.problem.__class__.__name__ == 'TSP':
            self.calculateNextGen_discrete()
        elif self.problem.__class__.__name__ == 'FunctionOptimization':
            self.calculateNextGen_continuous()

        self.w = self.w * self.damping

        self.fitness = self.problem.calculate_fitness(self.agents)
        i = np.where(self.fitness < self.personalBest)
        self.personalBest[i] = self.fitness[i]
        self.personalBestPosition[i] = self.agents[i]
        if self.personalBest.min() < self.globalBest:
            self.globalBest = self.personalBest.min()
            self.globalBestPosition = self.personalBestPosition[self.personalBest.argmin()]

        self.save_state()
