import numpy as np
from methods.method import Method
from enum import Enum
import math


class Selection(Enum):
    Rank = 0
    Roulette = 1


class GeneticAlgorithm(Method):
    def __init__(self, problem, num, selection_method=Selection.Rank, elite_size=0, mutation_rate=0.01):
        Method.__init__(self, problem, num)

        self.elite_size = elite_size
        self.mutation_rate = mutation_rate

        self.selection = self.rank_selection if selection_method == Selection.Rank else self.roulette_selection
        self.probabilities = None

        self.crossover = None
        self.mutation = None
        if problem.__class__.__name__ == 'TSP':
            self.crossover = self.crossover_permutation_ordered
            # self.crossover = self.crossover_permutation_cycle
            self.mutation = self.mutation_swap
        elif problem.__class__.__name__ == 'FunctionOptimization':
            self.crossover = self.crossover_continuous
            self.mutation = self.mutation_continuous

        self.sort_agents()

    def rank_selection(self):
        if self.probabilities is None:
            self.probabilities = np.fromfunction(lambda n: (self.num-1) - n + 1, (self.num,), dtype=int) / np.sum(np.arange(1, self.num+1))

        parents = np.zeros(self.agents.shape)
        parents[:self.elite_size] = self.agents[:self.elite_size]
        parents[self.elite_size:] = self.agents[np.random.choice(self.probabilities.shape[0], self.num-self.elite_size, replace=True, p=self.probabilities)]

        return parents

    def roulette_selection(self):
        # TODO roulette_selection
        return

    def crossover_permutation_cycle(self, parents):
        new_generation = np.zeros(self.agents.shape)
        new_generation[:self.elite_size] = parents[:self.elite_size] + 1

        pool = parents[np.random.choice(parents.shape[0], parents.shape[0], replace=False)] + 1

        for i in range(parents.shape[0] - self.elite_size):
            current_index = np.random.randint(pool.shape[1])
            parent1 = pool[i].copy()
            parent2 = pool[pool.shape[0] - 1 - i].copy()

            parent1[current_index], parent2[current_index] = parent2[current_index], parent1[current_index]
            current_value = parent1[current_index]

            temp = np.where(parent1 == current_value)[0]
            while temp.size != 1:
                current_index = temp[temp != current_index]
                parent1[current_index], parent2[current_index] = parent2[current_index], parent1[current_index]
                current_value = parent1[current_index]
                temp = np.where(parent1 == current_value)[0]
            new_generation[self.elite_size + i] = parent1

        new_generation = new_generation - 1
        return new_generation

    def crossover_permutation_ordered(self, parents):
        new_generation = np.zeros(self.agents.shape)
        new_generation[:self.elite_size] = parents[:self.elite_size] + 1

        pool = parents[np.random.choice(parents.shape[0], parents.shape[0], replace=False)] + 1

        for i in range(parents.shape[0] - self.elite_size):
            random = np.random.randint(pool.shape[1], size=2)
            start = random.min()
            end = random.max()
            parent1 = pool[i]
            parent2 = pool[pool.shape[0] - 1 - i]
            new_generation[self.elite_size + i][start:end+1] = parent1[start:end+1]
            diff = parent2[~np.in1d(parent2, new_generation[self.elite_size + i][start:end+1])]
            new_generation[self.elite_size + i][:start] = diff[:start]
            new_generation[self.elite_size + i][end+1:] = diff[start:]

        new_generation = new_generation - 1
        return new_generation

    def crossover_binary(self, parents):
        # TODO crossover_binary
        return

    def crossover_continuous(self, parents):
        new_generation = np.zeros(self.agents.shape)
        new_generation[:self.elite_size] = parents[:self.elite_size]

        pool = parents[np.random.choice(parents.shape[0], parents.shape[0], replace=False)]

        for i in range(parents.shape[0] - self.elite_size):
            random = np.random.rand()
            parent1 = pool[i]
            parent2 = pool[pool.shape[0] - 1 - i]
            new_generation[self.elite_size + i] = np.array([(1 - random) * parent1[0] + random * parent2[0], (1 - random) * parent1[1] + random * parent2[1]])

        return new_generation

    def mutation_continuous(self, pool):
        mutated = np.copy(pool)
        for i in range(pool.shape[0]):
            random = np.random.rand()
            if random < self.mutation_rate:
                mutated[i] = np.random.uniform(self.problem.lowerBound, self.problem.upperBound, 2)
        return mutated

    def mutation_swap(self, pool):
        # TODO change mutation_rate (not every chromosome) ?????
        mutated = np.copy(pool)
        for i in range(pool.shape[0]):
            for j in range(pool.shape[1]):
                random = np.random.rand()
                if random < self.mutation_rate:
                    swap = np.random.randint(pool.shape[1])
                    mutated[i][[j, swap]] = mutated[i][[swap, j]]
        return mutated

    def calculateNextGen(self):
        parents = self.selection()

        new_generation = self.crossover(parents)
        new_generation = self.mutation(new_generation)

        self.agents = new_generation
        self.fitness = self.problem.calculate_fitness(self.agents)

        self.sort_agents()
        self.save_state()

