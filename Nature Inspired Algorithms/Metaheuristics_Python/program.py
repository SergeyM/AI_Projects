import plotting

from methods.pso import PSO
from methods.firefly import Firefly
from methods.genetic import GeneticAlgorithm

import problems.functions as functions
from problems.function_optimization import FunctionOptimization
from problems.tsp import TSP


function_problem1 = FunctionOptimization(functions.Function.Rastrigin, -5.12, 5.12)
function_problem2 = FunctionOptimization(functions.Function.Drop_Wave, -5.12, 5.12)
function_problem3 = FunctionOptimization(functions.Function.Ackley, -32.768, 32.768)
function_problem4 = FunctionOptimization(functions.Function.Sphere, -5.12, 5.12)

tsp_problem1 = TSP('_berlin52.tsp')
tsp_problem2 = TSP('_ulysses22.tsp')
tsp_problem3 = TSP('_swiss42.tsp')
tsp_problem4 = TSP('_gr666.tsp')

# change the problem parameter to another one
# you can also change the hyperparameter
pso_tsp = PSO(tsp_problem1, 100, c1=2, c2=2, w=1)
ga_tsp = GeneticAlgorithm(tsp_problem1, 200, mutation_rate=0.001, elite_size=1)
fa_tsp = Firefly(tsp_problem1, 50, alpha=0.5, beta=1, gamma=0.05)

pso_function = PSO(function_problem1, 100, c1=2, c2=2, w=1)
ga_function = GeneticAlgorithm(function_problem1, 100, mutation_rate=0.05, elite_size=10)
fa_function = Firefly(function_problem1, 50, alpha=0.2, beta=0.5, gamma=0.1)

# change model in plotting function
x, y, z = functions.calculateFunction(function_problem1.function, function_problem1.lowerBound, function_problem1.upperBound)
plotting.plot_function(plotting.PlotMode.all_flags, x, y, z, model=pso_function, animate=True)
plotting.plot_tsp_network(tsp_problem1.cities, tsp_problem1.header, model=pso_tsp)