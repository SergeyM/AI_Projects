from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import animation
import numpy as np
import os
import networkx as nx
from flags import Flags
#from mpl_toolkits.basemap import Basemap
from itertools import chain
from problems.tsp_data.tsp_parser import read_tsp


class PlotMode(Flags):
    twoD = ()
    threeD = ()
    graph = ()


def create_plot3D(row, col, index, x, y, z):
    subplot = plt.subplot(row, col, index, projection='3d')

    # subplot.set_xlim3d(x.min(), x.max())
    # subplot.set_ylim3d(y.min(), y.max())

    # plot surface
    subplot.plot_surface(x, y, z, cmap=cm.get_cmap("jet", 50), rstride=8, cstride=8, alpha=0.5)

    # cset = subplot.contourf(x, y, z, zdir='z', offset=-1, cmap=cm.get_cmap("jet", 50))
    # cset = subplot.contourf(x, y, z, zdir='x', offset=minVal-1, cmap=cm.get_cmap("jet", 50))
    # cset = subplot.contourf(x, y, z, zdir='y', offset=maxVal+1, cmap=cm.get_cmap("jet", 50))

    subplot.set_xlabel('x')
    subplot.set_ylabel('y')
    subplot.set_zlabel('z')

    return subplot


def create_plot2D(row, col, index, x, y, z):
    subplot = plt.subplot(row, col, index)

    subplot.set_xlim(x.min(), x.max())
    subplot.set_ylim(y.min(), y.max())
    img = subplot.imshow(z, cmap=cm.get_cmap("jet", 50), extent=[x.min(), x.max(), y.min(), y.max()])
    img.set_interpolation('bilinear')

    return subplot


def create_graph(row, col, index):
    graph = plt.subplot(row, col, index)
    graph.set_autoscale_on(True)
    [best] = graph.plot(np.array([]), np.array([]))
    [average] = graph.plot(np.array([]), np.array([]))
    [worst] = graph.plot(np.array([]), np.array([]))
    return graph, best, average, worst


def plot_function(mode, x, y, z, model=None, animate=True):
    fig = plt.figure()

    subplot3_d = None
    subplot2_d = None

    if mode.twoD and mode.threeD and mode.graph:
        subplot3_d = create_plot3D(2, 2, 1, x, y, z)
        subplot2_d = create_plot2D(2, 2, 2, x, y, z)
        subplot_graph, best, average, worst = create_graph(2, 1, 2)
    elif mode.twoD and mode.threeD:
        subplot3_d = create_plot3D(1, 2, 1, x, y, z)
        subplot2_d = create_plot2D(1, 2, 2, x, y, z)
    elif mode.twoD and mode.graph:
        subplot2_d = create_plot2D(1, 2, 1, x, y, z)
        subplot_graph, best, average, worst = create_graph(1, 2, 2)
    elif mode.threeD and mode.graph:
        subplot3_d = create_plot3D(1, 2, 1, x, y, z)
        subplot_graph, best, average, worst = create_graph(1, 2, 2)
    elif mode.twoD:
        subplot2_d = create_plot2D(1, 1, 1, x, y, z)
    elif mode.threeD:
        subplot3_d = create_plot3D(1, 1, 1, x, y, z)

    fig.colorbar(subplot2_d.images[0] if mode.twoD else subplot3_d.collections[0], shrink=0.6, aspect=15, ax=subplot2_d if mode.twoD else subplot3_d)

    fig.suptitle('Funktionname: {}      Iteration: {}'.format(model.problem.function.__name__, 0))

    def init():
        return

    def update(f):
        model.calculateNextGen()
        coord = model.agents.transpose()
        if mode.twoD:
            scatter2_d.set_offsets(np.c_[coord[0], coord[1]])
        if mode.threeD:
            scatter3_d._offsets3d = (coord[0], coord[1], model.problem.function(coord))
        if mode.graph:
            x_data = np.arange(0, f+1, 1)
            best.set_data(x_data, model.best)
            average.set_data(x_data, model.average)
            worst.set_data(x_data, model.worst)
            subplot_graph.relim()
            subplot_graph.autoscale_view()

        fig.canvas.draw()
        fig.canvas.flush_events()

        fig.suptitle('Funktionname: {}      Iteration: {}'.format(model.problem.function.__name__, f))

    if model is not None:
        coord = model.agents.transpose()
        if mode.twoD:
            scatter2_d = subplot2_d.scatter(coord[0], coord[1], c='black', s=4)
        if mode.threeD:
            scatter3_d = subplot3_d.scatter(coord[0], coord[1], model.problem.function(coord), color='black', s=4)
        if animate:
            ani = animation.FuncAnimation(fig, update, init_func=init)

    plt.show()


def plot_tsp_network(coords, header, model=None):
    # TODO plot Map with Basemap
    if header['EDGE_WEIGHT_TYPE'] == 'GEO':
        """
        fig = plt.figure(figsize=(8, 6), edgecolor='w')
        m = Basemap(projection='cyl', resolution=None,
            llcrnrlat=-90, urcrnrlat=90,
            llcrnrlon=-180, urcrnrlon=180, )
        draw_map(m)
        """
        pass

    fig = plt.figure()

    filename = header['filename'] + '.opt.tour'
    absolut_path = os.path.dirname(__file__)
    if os.path.isfile(os.path.join(absolut_path, 'problems/tsp_data/optimal_routes/' + filename)):
        optimal_path_header, optimal_solution = read_tsp(filename)
        optimal_solution = optimal_solution - 1
        optimal_path = coords[optimal_solution]
        optimal_path = np.vstack((optimal_path, coords[optimal_solution[0]]))
        optimal_path = optimal_path.transpose()
        optimal_subplot = plt.subplot(2, 2, 2)
        [optimal_line] = optimal_subplot.plot(optimal_path[0], optimal_path[1])
        scatter_optimal = optimal_subplot.scatter(coords.transpose()[0], coords.transpose()[1], c='black', s=4)
        subplot = plt.subplot(2, 2, 1)
    else:
        subplot = plt.subplot(2, 1, 1)
        print('No data for optimal solution!')

    scatter = subplot.scatter(coords.transpose()[0], coords.transpose()[1], c='black', s=4)

    if model is not None:
        path = coords[model.bestSolution.astype(int)]
        path = np.vstack((path, coords[model.bestSolution[0].astype(int)]))
        path = path.transpose()

        [line] = subplot.plot(path[0], path[1])

        graph, best, average, worst = create_graph(2, 1, 2)

    def init():
        return

    def update(f):
        model.calculateNextGen()

        path = coords[model.bestSolution.astype(int)]
        path = np.vstack((path, coords[model.bestSolution[0].astype(int)]))
        path = path.transpose()
        line.set_data(path[0], path[1])

        x_data = np.arange(0, f+1, 1)
        best.set_data(x_data, model.best)
        average.set_data(x_data, model.average)
        worst.set_data(x_data, model.worst)
        graph.relim()
        graph.autoscale_view()

        fig.canvas.draw()
        fig.canvas.flush_events()

        plt.suptitle('name: {}, iteration: {}, distance: {}'.format(header['NAME'], f, model.bestFitness))

    if model is not None:
        ani = animation.FuncAnimation(fig, update, init_func=init)

    plt.show()


def draw_map(m, scale=0.2):
    # draw a shaded-relief image
    m.shadedrelief(scale=scale)

    # lats and longs are returned as a dictionary
    lats = m.drawparallels(np.linspace(-90, 90, 13))
    lons = m.drawmeridians(np.linspace(-180, 180, 13))

    # keys contain the plt.Line2D instances
    lat_lines = chain(*(tup[1][0] for tup in lats.items()))
    lon_lines = chain(*(tup[1][0] for tup in lons.items()))
    all_lines = chain(lat_lines, lon_lines)

    # cycle through these lines and set the desired style
    for line in all_lines:
        line.set(linestyle='-', alpha=0.3, color='w')
