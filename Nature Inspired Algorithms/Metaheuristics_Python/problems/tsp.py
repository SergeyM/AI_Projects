from problems.problem import Problem
import problems.tsp_data.tsp_parser as parser
import numpy as np

# TODO Initialization with Nearest Neighbor

class TSP(Problem):
    def __init__(self, filename):
        Problem.__init__(self)
        self.header, self.distances, self.cities = parser.read_tsp(filename)

    def generate_population(self, num):
        return np.argsort(np.random.rand(num, self.cities.shape[0]), axis=1)

    def calculate_fitness(self, agents):
        return self.route_distance_function(agents)

    def route_distance_function(self, routes):
        totalDistances = np.zeros(routes.shape[0])

        for i in range(routes.shape[0]):
            prev = routes[i][-1]
            for j in range(routes[i].size):
                totalDistances[i] += self.distances[int(prev)][int(routes[i][j])]
                prev = routes[i][j]

        return totalDistances


