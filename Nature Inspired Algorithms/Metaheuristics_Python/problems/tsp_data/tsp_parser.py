from scipy.spatial import distance
from geopy.distance import geodesic
from sklearn import manifold
import numpy as np
import os


def read_tsp(filename):
    file = None

    absolut_path = os.path.dirname(__file__)
    if filename.find('.tsp') != -1:
        file = open(os.path.join(absolut_path, 'tsp_problems/' + filename), 'r')
    elif filename.find('.tour') != -1:
        file = open(os.path.join(absolut_path, 'optimal_routes/' + filename), 'r')

    header = {}
    dist_function = None
    read_function = read_coordinates
    dist = None
    city_coords = None

    header['filename'] = filename.split('.')[0]

    line = file.readline().strip()
    while line and line.find('SECTION') == -1:
        if line.find('NAME') != -1:
            header['NAME'] = line.split(':')[1].strip()
        elif line.find('TYPE') == 0:
            header['TYPE'] = line.split(':')[1].strip()
            if header['TYPE'].find('TOUR') != -1:
                read_function = read_path
        elif line.find('DIMENSION') != -1:
            header['DIMENSION'] = line.split(':')[1].strip()
        elif line.find('EDGE_WEIGHT_TYPE') != -1:
            header['EDGE_WEIGHT_TYPE'] = line.split(':')[1].strip()

            if header['EDGE_WEIGHT_TYPE'].find('EUC') != -1:
                dist_function = euclidean_distances
            elif header['EDGE_WEIGHT_TYPE'].find('MAN') != -1:
                dist_function = manhattan_distances
            elif header['EDGE_WEIGHT_TYPE'].find('MAX') != -1:
                dist_function = maximum_distances
            elif header['EDGE_WEIGHT_TYPE'].find('GEO') != -1:
                dist_function = geographical_distances
            # TODO Pseudo-Euclidean, Ceiling
            elif header['EDGE_WEIGHT_TYPE'] == 'EXPLICIT':
                while line.find('EDGE_WEIGHT_FORMAT') == -1:
                    line = file.readline().strip()
                header['EDGE_WEIGHT_FORMAT'] = line.split(':')[1].strip()
                if header['EDGE_WEIGHT_FORMAT'].find('FULL_MATRIX') != -1:
                    read_function = read_full_matrix
                # TODO UPPER_ROW, LOWER_ROW, UPPER_DIAG_ROW, LOWER_DIAG_ROW
                # TODO UPPER COL, LOWER_COL, UPPER_DIAG_COL, LOWER_DIAG_COL

        line = file.readline().strip()

    if line.find('NODE_COORD_SECTION') != -1:
        dist, city_coords = read_function(file, dist_function)
    elif line.find('EDGE_WEIGHT_SECTION') != -1:
        dist, city_coords = read_function(file)
    elif line.find('TOUR_SECTION') != -1:
        path = read_function(file)
        return header, path

    return header, dist, city_coords


def read_path(file):
    line = file.readline()
    path = np.array(line.split()).astype(np.int)
    while True:
        line = file.readline()
        if not line or line.isspace() or line.find('EOF') != -1 or line.find('-1') != -1:
            break
        path = np.append(path, np.array(line.split()).astype(np.int))
    file.close()
    return path


def read_coordinates(file, dist_function):
    line = file.readline()
    coord = np.array(list(map(float, line.split())))[1:]
    while True:
        line = file.readline()
        if not line or line.isspace() or line.find('EOF') != -1:
            break
        coord = np.vstack((coord, np.array(list(map(float, line.split())))[1:]))
    dist = dist_function(coord)
    file.close()
    return dist, coord


def read_full_matrix(file):
    line = file.readline()
    dist = np.array(list(map(float, line.split())))
    while True:
        line = file.readline()
        if not line or line.isspace() or line.find('EOF') != -1:
            break
        dist = np.vstack((dist, np.array(list(map(float, line.split())))))

    max_dist = np.amax(dist)
    norm_dist = dist / max_dist

    mds = manifold.MDS(n_components=2, dissimilarity="precomputed", random_state=6)
    coords = mds.fit(norm_dist).embedding_
    file.close()
    return dist, coords


def euclidean_distances(coordinates):
    return distance.squareform(distance.pdist(coordinates, 'euclidean'))


def manhattan_distances(coordinates):
    return distance.squareform(distance.pdist(coordinates, 'cityblock'))


def maximum_distances(coordinates):
    return distance.squareform(distance.pdist(coordinates, 'chebyshev'))


def geographical_distances(coordinates):
    return distance.squareform(distance.pdist(coordinates, lambda u, v: geodesic(u, v).kilometers))



