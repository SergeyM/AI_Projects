import numpy as np
from enum import Enum


def rastrigin(grid):
    """
    :param grid: shape of (dimension d, x1,...,xd)
    Input domain usually [-5.12, 5.12]
    Global Minimum: f(x*) = 0, at x* = (0,...,0)
    :return: f(x1,...,xd)
    """
    return 10*grid.shape[0] + np.sum(grid**2 - 10*np.cos(2*np.pi*grid), axis=0)


def drop_wave(grid):
    """
    :param grid: shape of (2, x, y) - only 2D input is allowed
    Input domain usually [-5.12, 5.12]
    Global Minimum: f(x*) = -1, at x* = (0,0)
    :return: f(x1,...,xd)
    """
    return -(1+np.cos(12*np.sqrt(grid[0]**2+grid[1]**2)))/(0.5*(grid[0]**2+grid[1]**2)+2)


def ackley(grid, a=20, b=0.2, c=2*np.pi):
    """
    :param grid: shape of (dimension d, x1,...,xd)
    :param a: recommended variable value: 20
    :param b: recommended variable value: 0.2
    :param c: recommended variable value: 2 * pi
    Input domain usually [-32.768, 32.768]
    Global Minimum: f(x*) = 0, at x* = (0,0)
    :return: f(x1,...,xd)
    """
    d = grid.shape[0]
    return -a * np.exp(-b * np.sqrt((1/d) * np.sum(grid**2, axis=0))) - np.exp((1/d) * np.sum(np.cos(c * grid), axis=0)) + a + np.exp(1)


def sphere(grid):
    """
    :param grid: shape of (dimension d, x1,...,xd)
    Input domain usually [-5.12, 5.12]
    Global Minimum: f(x*) = 0, at x* = (0,...,0)
    :return: f(x1,...,xd)
    """
    return np.sum(grid**2, axis=0)


class Function(Enum):
    Rastrigin = rastrigin
    Drop_Wave = drop_wave
    Ackley = ackley
    Sphere = sphere


def calculateFunction(func, lb, ub, samples=200):
    x = np.linspace(lb, ub, samples, endpoint=True)
    y = np.linspace(lb, ub, samples, endpoint=True)

    grid = np.ndarray((2, x.size, y.size))
    grid[0], grid[1] = np.meshgrid(x, y)

    z = func(grid)
    return grid[0], grid[1], z

