from problems.problem import Problem
import numpy as np


class FunctionOptimization(Problem):
    def __init__(self, function, lowerBound, upperBound, dimension=2):
        Problem.__init__(self)
        self.lowerBound = lowerBound
        self.upperBound = upperBound
        self.dimension = dimension
        self.function = function

    def generate_population(self, num):
        return np.random.uniform(self.lowerBound, self.upperBound, (num, self.dimension))

    def calculate_fitness(self, agents):
        return self.function(agents.transpose())
