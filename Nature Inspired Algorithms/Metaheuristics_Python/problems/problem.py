from abc import ABC, abstractmethod


class Problem(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def generate_population(self, num):
        pass

    @abstractmethod
    def calculate_fitness(self, agents):
        pass
