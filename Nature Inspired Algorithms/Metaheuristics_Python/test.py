import plotting
import itertools

from methods.pso import PSO
from methods.firefly import Firefly
from methods.genetic import GeneticAlgorithm

import problems.functions as functions
from problems.function_optimization import FunctionOptimization
from problems.tsp import TSP
from problems.tsp_data.tsp_parser import read_tsp
import matplotlib.pyplot as plt

import numpy as np

function_problem = FunctionOptimization(functions.Function.Rastrigin, -5.12, 5.12)
function_problem2 = FunctionOptimization(functions.Function.Drop_Wave, -5.12, 5.12)
#problem = TSP('_swiss42.tsp')
tsp_problem = TSP('_berlin52.tsp')
tsp_problem2 = TSP('_ulysses22.tsp')
"""
best_list_function = []
best_fitness_function = []

best_list_tsp_pso1 = []
best_list_tsp_pso2 = []

best_list_tsp_ga1 = []
best_list_tsp_ga2 = []

best_list_tsp_fa1 = []
best_list_tsp_fa2 = []

for i in range(40):
    print(i)
    pso = PSO(function_problem, 100, c1=2, c2=2, w=1)
    ga = GeneticAlgorithm(function_problem, 100, mutation_rate=0.05, elite_size=10)
    fa = Firefly(function_problem, 50, alpha=0.2, beta=0.5, gamma=0.1)

    pso2 = PSO(function_problem2, 100, c1=2, c2=2, w=1)
    ga2 = GeneticAlgorithm(function_problem2, 100, mutation_rate=0.05, elite_size=10)
    fa2 = Firefly(function_problem2, 50, alpha=0.2, beta=0.5, gamma=0.1)
    for i in range(200):
        pso.calculateNextGen()
        ga.calculateNextGen()
        fa.calculateNextGen()

        pso2.calculateNextGen()
        ga2.calculateNextGen()
        fa2.calculateNextGen()

    best_list_tsp_pso1.append(pso.best.tolist())
    best_list_tsp_pso2.append(pso2.best.tolist())

    best_list_tsp_ga1.append(ga.best.tolist())
    best_list_tsp_ga2.append(ga2.best.tolist())

    best_list_tsp_fa1.append(fa.best.tolist())
    best_list_tsp_fa2.append(fa2.best.tolist())

plt.figure()
pso1_result = np.asarray(best_list_tsp_pso1).mean(axis=0)
ga1_result = np.asarray(best_list_tsp_ga1).mean(axis=0)
fa1_result = np.asarray(best_list_tsp_fa1).mean(axis=0)
x = np.arange(1, 201, dtype=np.int)
plt.plot(x, pso1_result)
plt.plot(x, ga1_result)
plt.plot(x, fa1_result)

plt.legend(['pso', 'ga', 'fa'])
plt.savefig('comparison_rastrigin.png')

plt.figure()
pso2_result = np.asarray(best_list_tsp_pso2).mean(axis=0)
ga2_result = np.asarray(best_list_tsp_ga2).mean(axis=0)
fa2_result = np.asarray(best_list_tsp_fa2).mean(axis=0)
x = np.arange(1, 201, dtype=np.int)
plt.plot(x, pso2_result)
plt.plot(x, ga2_result)
plt.plot(x, fa2_result)

plt.legend(['pso', 'ga', 'fa'])
plt.savefig('comparison_dropwave.png')
"""

"""
for i in range(10):
    print(i)
    pso_tsp = PSO(tsp_problem, 100, c1=2, c2=2, w=1)
    ga_tsp = GeneticAlgorithm(tsp_problem, 200, mutation_rate=0.001, elite_size=1)
    fa_tsp = Firefly(tsp_problem, 50, alpha=0.5, beta=1, gamma=0.05)

    pso_tsp2 = PSO(tsp_problem2, 100, c1=2, c2=2, w=1)
    ga_tsp2 = GeneticAlgorithm(tsp_problem2, 200, mutation_rate=0.001, elite_size=1)
    fa_tsp2 = Firefly(tsp_problem2, 50, alpha=0.5, beta=1, gamma=0.05)
    for i in range(300):
        #pso_tsp.calculateNextGen()
        #ga_tsp.calculateNextGen()
        #fa_tsp.calculateNextGen()

        pso_tsp2.calculateNextGen()
        ga_tsp2.calculateNextGen()
        fa_tsp2.calculateNextGen()

    #best_list_tsp_pso1.append(pso_tsp.best.tolist())
    best_list_tsp_pso2.append(pso_tsp2.best.tolist())

    #best_list_tsp_ga1.append(ga_tsp.best.tolist())
    best_list_tsp_ga2.append(ga_tsp2.best.tolist())

    #best_list_tsp_fa1.append(fa_tsp.best.tolist())
    best_list_tsp_fa2.append(fa_tsp2.best.tolist())

plt.figure()
pso1_result = np.asarray(best_list_tsp_pso1).mean(axis=0)
ga1_result = np.asarray(best_list_tsp_ga1).mean(axis=0)
fa1_result = np.asarray(best_list_tsp_fa1).mean(axis=0)
x = np.arange(1, 401, dtype=np.int)
plt.plot(x, pso1_result)
plt.plot(x, ga1_result)
plt.plot(x, fa1_result)

plt.legend(['pso', 'ga', 'fa'])
plt.savefig('comparison_berlin.png')

plt.figure()
pso2_result = np.asarray(best_list_tsp_pso2).mean(axis=0)
ga2_result = np.asarray(best_list_tsp_ga2).mean(axis=0)
fa2_result = np.asarray(best_list_tsp_fa2).mean(axis=0)
x = np.arange(1, 301, dtype=np.int)
plt.plot(x, pso2_result)
plt.plot(x, ga2_result)
plt.plot(x, fa2_result)

plt.legend(['pso', 'ga', 'fa'])
plt.savefig('comparison_ulysses.png')
"""

"""
num_iterations = 300
num_param = [5, 10, 20, 40]

pso_param_c = [1, 2, 3]
pso_param_w = [0.5, 1, 1.5, 2]

# num > ga_param_elitesize
ga_param_mutation = [0, 0.001, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2]
ga_param_elitesize = [0, 1, 5, 10, 20]

fa_param_alpha = [0, 0.2, 0.5, 1]
fa_param_beta = [0.1, 0.5, 1, 1.5, 2]
fa_param_gamma = [0.01, 0.05, 0.1, 0.5, 1, 10]

params_list = []

best_list_function = []
best_list_tsp = []

best_solution_function = []
best_solution_tsp = []

best_fitness_function = []
best_fitness_tsp = []
print("Particle Swarm Optimization")
for num, c1, c2, w in itertools.product(num_param, pso_param_c, pso_param_c, pso_param_w):
    print((num, c1, c2, w))
    params_list.append([num, c1, c2, w])
    pso_function = PSO(function_problem, num, c1=c1, c2=c2, w=w)
    pso_tsp = PSO(tsp_problem, num, c1=c1, c2=c2, w=w)
    for i in range(num_iterations):
        pso_function.calculateNextGen()
        pso_tsp.calculateNextGen()

    print(pso_function.bestFitness)
    print(pso_tsp.bestFitness)

    best_solution_function.append(pso_function.bestSolution.tolist())
    best_fitness_function.append(pso_function.bestFitness)
    best_list_function.append(pso_function.best.tolist())

    best_solution_tsp.append(pso_tsp.bestSolution.tolist())
    best_fitness_tsp.append(pso_tsp.bestFitness)
    best_list_tsp.append(pso_tsp.best.tolist())

f = open("pso_function.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_function), delimiter=', ', fmt='% 11.3f')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_function), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_function), delimiter=', ', fmt='% 11.3f')
f.close()

f = open("pso_tsp.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_tsp, dtype=np.int), delimiter=', ', fmt='% 3d')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_tsp), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_tsp), delimiter=', ', fmt='% 11.3f')
f.close()


params_list = []

best_list_function = []
best_list_tsp = []

best_solution_function = []
best_solution_tsp = []

best_fitness_function = []
best_fitness_tsp = []
print("Genetic Algorithm")
for num, mutation, elite in itertools.product(num_param, ga_param_mutation, ga_param_elitesize):
    if elite < num:
        print((num, mutation, elite))
        params_list.append([num, mutation, elite])     
        ga_function = GeneticAlgorithm(function_problem, num, mutation_rate=mutation, elite_size=elite)
        ga_tsp = GeneticAlgorithm(tsp_problem, num, mutation_rate=mutation, elite_size=elite)
        for i in range(num_iterations):
            ga_function.calculateNextGen()
            ga_tsp.calculateNextGen()
            
        print(ga_function.bestFitness)
        print(ga_tsp.bestFitness)
            
        best_solution_function.append(ga_function.bestSolution.tolist())
        best_fitness_function.append(ga_function.bestFitness)
        best_list_function.append(ga_function.best.tolist())

        best_solution_tsp.append(ga_tsp.bestSolution.tolist())
        best_fitness_tsp.append(ga_tsp.bestFitness)
        best_list_tsp.append(ga_tsp.best.tolist())

f = open("ga_function.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_function), delimiter=', ', fmt='% 11.3f')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_function), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_function), delimiter=', ', fmt='% 11.3f')
f.close()

f = open("ga_tsp.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_tsp, dtype=np.int), delimiter=', ', fmt='% 3d')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_tsp), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_tsp), delimiter=', ', fmt='% 11.3f')
f.close()


params_list = []

best_list_function = []
best_list_tsp = []

best_solution_function = []
best_solution_tsp = []

best_fitness_function = []
best_fitness_tsp = []
print("Firefly Algorithm")
counter = 0
for num, alpha, beta, gamma in itertools.product(num_param, fa_param_alpha, fa_param_beta, fa_param_gamma):
    print((num, alpha, beta, gamma))
    params_list.append([num, alpha, beta, gamma])   
    fa_function = Firefly(function_problem, num, alpha=alpha, beta=beta, gamma=gamma)
    fa_tsp = Firefly(tsp_problem, num, alpha=alpha, beta=beta, gamma=gamma)
    for i in range(num_iterations):
        fa_function.calculateNextGen()
        fa_tsp.calculateNextGen()
        
    print(fa_function.bestFitness)
    print(fa_tsp.bestFitness)
        
    best_solution_function.append(fa_function.bestSolution.tolist())
    best_fitness_function.append(fa_function.bestFitness)
    best_list_function.append(fa_function.best.tolist())

    best_solution_tsp.append(fa_tsp.bestSolution.tolist())
    best_fitness_tsp.append(fa_tsp.bestFitness)
    best_list_tsp.append(fa_tsp.best.tolist())

f = open("fa_function.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_function), delimiter=', ', fmt='% 11.3f')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_function), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_function), delimiter=', ', fmt='% 11.3f')
f.close()

f = open("fa_tsp.txt", 'wb')
f.write("Parameter\n (num, c1, c2, w)".encode('utf-8'))
np.savetxt(f, np.asarray(params_list), delimiter=', ', fmt='% 8.3f')
f.write("Best Solutions\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_solution_tsp, dtype=np.int), delimiter=', ', fmt='% 3d')
f.write("Best Fitness values\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_fitness_tsp), delimiter=', ', fmt='% 11.3f')
f.write("Fitness Graph\n".encode('utf-8'))
np.savetxt(f, np.asarray(best_list_tsp), delimiter=', ', fmt='% 11.3f')
f.close()
"""

#test = Bee(functions.Function.Rastrigin, 100, -5.12, 5.12)
#test = GeneticAlgorithm(problem, 200, mutation_rate=0.015, elite_size=20)
#test = GeneticAlgorithm(problem, 15, mutation_rate=0.2, elite_size=5)

#test.calculateNextGen()

#x, y, z = functions.calculateFunction(problem.function, problem.lowerBound, problem.upperBound)
#plotting.plot_function(plotting.PlotMode.twoD | plotting.PlotMode.graph, x, y, z, model=test, animate=True)
#plotting.plot_tsp_network(problem.cities, problem.header, test)
