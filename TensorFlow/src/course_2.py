import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import wget
import sys
import os
import zipfile

from utils import bar_progress, plot_history
from callbacks import CustomCallback

print(tf.__version__)
np.set_printoptions(linewidth=200)

# ---------------------------- DATA ----------------------------

""" Data 1: Cats and Dogs 
url_cats_dogs = "https://storage.googleapis.com/mledu-datasets/cats_and_dogs_filtered.zip"
path_cats_dogs = "../data/cats_and_dogs_filtered.zip"
if not os.path.isfile(path_cats_dogs):
    wget.download(url_cats_dogs, path_cats_dogs, bar=bar_progress)
with zipfile.ZipFile(path_cats_dogs, 'r') as zip_ref:
    zip_ref.extractall('../data/cats_and_dogs')
path_cats_dogs = '../data/cats_and_dogs/cats_and_dogs_filtered'

train_dir = os.path.join(path_cats_dogs, 'train')
validation_dir = os.path.join(path_cats_dogs, 'validation')
"""

url_rps = "https://storage.googleapis.com/laurencemoroney-blog.appspot.com/rps.zip"
url_rps_test = "https://storage.googleapis.com/laurencemoroney-blog.appspot.com/rps-test-set.zip"
path_rps = "../data/rps.zip"
path_rps_test = "../data/rps-test.zip"

if not os.path.isfile(path_rps):
    wget.download(url_rps, path_rps, bar=bar_progress)
with zipfile.ZipFile(path_rps, 'r') as zip_ref:
    zip_ref.extractall('../data/rps')


if not os.path.isfile(path_rps_test):
    wget.download(url_rps_test, path_rps_test, bar=bar_progress)
with zipfile.ZipFile(path_rps_test, 'r') as zip_ref:
    zip_ref.extractall('../data/rps-test')

path_rps = '../data/rps/rps'
path_rps_test = '../data/rps-test/rps-test-set'

train_dir = path_rps
validation_dir = path_rps_test

train_datagen = ImageDataGenerator(
    rescale=1./255.,
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest'
)
test_datagen = ImageDataGenerator(rescale=1./255.)

"""
train_generator = train_datagen.flow_from_directory(
    train_dir,
    batch_size=20,
    class_mode='binary',
    target_size=(150, 150)
)
validation_generator = test_datagen.flow_from_directory(
    validation_dir,
    batch_size=20,
    class_mode='binary',
    target_size=(150, 150)
)
"""

train_generator = train_datagen.flow_from_directory(
    train_dir,
    batch_size=126,
    class_mode='categorical',
    target_size=(150, 150)
)
validation_generator = test_datagen.flow_from_directory(
    validation_dir,
    batch_size=126,
    class_mode='categorical',
    target_size=(150, 150)
)

# ---------------------------- MODEL ----------------------------

""" Model 1: Previous model from course_1
model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(16, (3, 3), activation='relu', input_shape=(150, 150, 3)),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(32, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

""" Model 2: Transfer Learning with Inception v3
url_weights = "https://storage.googleapis.com/mledu-datasets/inception_v3_weights_tf_dim_ordering_tf_kernels_notop.h5"
path_weights = "../model_weights/inception_v3_weights_tf_dim_ordering_tf_kernels_notop.h5"
if not os.path.isfile(path_weights):
    wget.download(url_weights, path_weights, bar=bar_progress)

from tensorflow.keras.applications.inception_v3 import InceptionV3
pre_trained_model = InceptionV3(
    input_shape=(150, 150, 3),
    include_top=False,
    weights=None
)
pre_trained_model.load_weights(path_weights)
for layer in pre_trained_model.layers:
    layer.trainable = False

last_layer = pre_trained_model.get_layer('mixed7')
last_output = last_layer.output
x = tf.keras.layers.Flatten()(last_output)
x = tf.keras.layers.Dense(1024, activation='relu')(x)
x = tf.keras.layers.Dropout(0.2)(x)
x = tf.keras.layers.Dense(1, activation='sigmoid')(x)

model = tf.keras.Model(pre_trained_model.input, x)
"""

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=(150, 150, 3)),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(128, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(3, activation='softmax')
])

model.summary()

# ---------------------------- COMPILE ----------------------------

"""
from tensorflow.keras.optimizers import RMSprop
model.compile(
    optimizer=RMSprop(lr=0.0001),
    loss='binary_crossentropy',
    metrics=['accuracy']
)
"""

model.compile(
    loss='categorical_crossentropy',
    optimizer='rmsprop',
    metrics=['accuracy']
)

# ---------------------------- FIT, EVALUATE, PREDICT ----------------------------

"""
history = model.fit(
    train_generator,
    validation_data=validation_generator,
    steps_per_epoch=100,  # 2000 images = batch_size * steps
    epochs=20,
    validation_steps=50,  # 1000 images = batch_size * steps
    verbose=1
)
"""

history = model.fit(
    train_generator,
    validation_data=validation_generator,
    steps_per_epoch=20,
    epochs=25,
    validation_steps=3,
    verbose=1
)

# ---------------------------- EXTRA ----------------------------

plot_history(history)
