import csv

import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.optimizers import Adam
import tensorflow_datasets as tfds
import numpy as np
import wget
import sys
import os
import zipfile
import json
import io

from tensorflow.python.keras import regularizers

from utils import bar_progress, plot_history
from callbacks import CustomCallback

print(tf.__version__)
np.set_printoptions(linewidth=200)

# ---------------------------- HYPER PARAMETERS ----------------------------

vocabulary_size = 10000
embedding_dim = 100
oov_token = "<OOV>"
padding_mode = "pre"
trunc_mode = "post"
max_sentence_length = 120
num_epochs = 200

# ---------------------------- DATA ----------------------------

# Stopwords list from https://github.com/Yoast/YoastSEO.js/blob/develop/src/config/stopwords.js
stopwords = ["a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "as", "at",
             "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "could", "did", "do",
             "does", "doing", "down", "during", "each", "few", "for", "from", "further", "had", "has", "have", "having",
             "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how",
             "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "it", "it's", "its", "itself",
             "let's", "me", "more", "most", "my", "myself", "nor", "of", "on", "once", "only", "or", "other", "ought",
             "our", "ours", "ourselves", "out", "over", "own", "same", "she", "she'd", "she'll", "she's", "should",
             "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then",
             "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through",
             "to", "too", "under", "until", "up", "very", "was", "we", "we'd", "we'll", "we're", "we've", "were",
             "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why",
             "why's", "with", "would", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself",
             "yourselves"]

"""" Data 1
path = "../data/sarcasm.json"
if not os.path.isfile(path):
    wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/sarcasm.json", path, bar=bar_progress)
with open(path, 'r') as f:
    datastore = json.load(f)

sentences = []
labels = []
urls = []
for item in datastore:
    sentences.append(item['headline'])
    labels.append(item['is_sarcastic'])
    urls.append(item['article_link'])

training_sentences = sentences[:20000]
training_labels = np.array(labels[:20000])
testing_sentences = sentences[20000:]
testing_labels = np.array(labels[20000:])
"""

""" Data 2
path = "../data/bbc-text.csv"
if not os.path.isfile(path):
    wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/bbc-text.csv", path, bar=bar_progress)

sentences = []
labels = []
with open(path, 'r') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader)
    for row in reader:
        labels.append(row[0])
        sentence = row[1]
        for word in stopwords:
            token = " " + word + " "
            sentence = sentence.replace(token, " ")
            sentence = sentence.replace("  ", " ")
        sentences.append(sentence)
"""

""" Data 3
# https://www.tensorflow.org/datasets/catalog/overview
imdb, info = tfds.load("imdb_reviews", with_info=True, as_supervised=True)
training_sentences = []
training_labels = []
testing_sentences = []
testing_labels = []

for s, l in imdb['train']:
    training_sentences.append(s.numpy().decode('utf8'))
    training_labels.append(l.numpy())

for s, l in imdb['test']:
    testing_sentences.append(s.numpy().decode('utf8'))
    testing_labels.append(l.numpy())

training_labels = np.array(training_labels)
testing_labels = np.array(testing_labels)
"""

# tokenizer = Tokenizer(num_words=vocabulary_size, oov_token=oov_token)
# tokenizer.fit_on_texts(training_sentences)
# word_index = tokenizer.word_index

# training_sequences = tokenizer.texts_to_sequences(training_sentences)
# training_padded = pad_sequences(training_sequences, maxlen=max_sentence_length, truncating=trunc_mode, padding=padding_mode)
# testing_sequences = tokenizer.texts_to_sequences(testing_sentences)
# testing_padded = pad_sequences(testing_sequences, maxlen=max_sentence_length, truncating=trunc_mode, padding=padding_mode)

# label_tokenizer = Tokenizer()
# label_tokenizer.fit_on_texts(labels)
# label_word_index = label_tokenizer.word_index
# label_seq = label_tokenizer.texts_to_sequences(labels)


# path = "../data/irish-lyrics-eof.txt"
# if not os.path.isfile(path):
#     wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/irish-lyrics-eof.txt", path, bar=bar_progress)

path = "../data/sonnets.txt"
if not os.path.isfile(path):
    wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/sonnets.txt", path, bar=bar_progress)

data = open(path).read()
corpus = data.lower().split("\n")

tokenizer = Tokenizer()
tokenizer.fit_on_texts(corpus)
word_index = tokenizer.word_index

input_sequences = []
for line in corpus:
    token_list = tokenizer.texts_to_sequences([line])[0]
    for i in range(1, len(token_list)):
        n_gram_sequence = token_list[:i+1]
        input_sequences.append(n_gram_sequence)
max_sentence_length = max([len(x) for x in input_sequences])
input_sequences = np.array(pad_sequences(input_sequences, maxlen=max_sentence_length, padding='pre'))

xs, labels = input_sequences[:, :-1], input_sequences[:, -1]
ys = tf.keras.utils.to_categorical(labels, num_classes=len(word_index)+1)

"""
imdb, info = tfds.load("imdb_reviews/subwords8k", with_info=True, as_supervised=True)
train_data, test_data = imdb['train'], imdb['test']
tokenizer = info.features['text'].encoder

sample_string = 'TensorFlow, from basics to mastery'
tokenized_string = tokenizer.encode(sample_string)
print('Tokenized string is {}'.format(tokenized_string))
original_string = tokenizer.decode(tokenized_string)
print('The original string: {}'.format(original_string))

BUFFER_SIZE = 10000
BATCH_SIZE = 64
train_dataset = train_data.shuffle(BUFFER_SIZE)
train_dataset = train_dataset.padded_batch(BATCH_SIZE, tf.compat.v1.data.get_output_shapes(train_dataset))
test_dataset = test_data.padded_batch(BATCH_SIZE, tf.compat.v1.data.get_output_shapes(test_data))
"""

# ---------------------------- MODEL ----------------------------

""" Model 1
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(vocabulary_size, embedding_dim, input_length=max_sentence_length),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(6, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

"""
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(tokenizer.vocab_size, embedding_dim, input_length=max_sentence_length),
    tf.keras.layers.GlobalAveragePooling1D(),
    tf.keras.layers.Dense(24, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

"""
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(tokenizer.vocab_size, 64),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences=True)),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

"""
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(tokenizer.vocab_size, 64),
    tf.keras.layers.Conv1D(128, 5, activation='relu'),
    tf.keras.layers.GlobalAveragePooling1D(),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

"""
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(vocabulary_size, embedding_dim, input_length=max_sentence_length),
    tf.keras.layers.Bidirectional(tf.keras.layers.GRU(32)),
    tf.keras.layers.Dense(6, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])
"""

"""
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(len(word_index)+1, embedding_dim, input_length=max_sentence_length-1),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(150)),
    tf.keras.layers.Dense(len(word_index)+1, activation='softmax')
])
"""

model = tf.keras.Sequential([
    tf.keras.layers.Embedding(len(word_index)+1, embedding_dim, input_length=max_sentence_length-1),
    tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(150, return_sequences = True)),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.LSTM(100),
    tf.keras.layers.Dense((len(word_index)+1)//2, activation='relu', kernel_regularizer=regularizers.l2(0.01)),
    tf.keras.layers.Dense(len(word_index)+1, activation='softmax')
])

# ---------------------------- COMPILE ----------------------------

# model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

adam = Adam(lr=0.001)
model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

model.summary()

# ---------------------------- FIT, EVALUATE, PREDICT ----------------------------

# history = model.fit(training_padded, training_labels, epochs=num_epochs, validation_data=(testing_padded, testing_labels))

# history = model.fit(train_dataset, epochs=num_epochs, validation_data=test_dataset)

# history = model.fit(train_dataset, epochs=num_epochs, validation_data=test_dataset)

history = model.fit(xs, ys, epochs=num_epochs)

# ---------------------------- EXTRA ----------------------------

plot_history(history)

# https://projector.tensorflow.org/

embedding_layer = model.layers[0]
weights = embedding_layer.get_weights()[0]  # shape: (vocab_size, embedding_dim)
reverse_word_index = dict([(value, key) for (key, value) in tokenizer.word_index.items()])

out_v = io.open('../temp/vecs.tsv', 'w', encoding='utf-8')
out_m = io.open('../temp/meta.tsv', 'w', encoding='utf-8')
for word_num in range(1, vocabulary_size):
    word = reverse_word_index[word_num]
    embeddings = weights[word_num]
    out_m.write(word + "\n")
    out_v.write('\t'.join([str(x) for x in embeddings]) + "\n")
out_v.close()
out_m.close()

# ----------------------------

seed_text = "I've got a bad feeling about this"
next_words = 100

for _ in range(next_words):
    token_list = tokenizer.texts_to_sequences([seed_text])[0]
    token_list = pad_sequences([token_list], maxlen=max_sentence_length-1, padding='pre')
    predicted = model.predict_classes(token_list, verbose=0)
    output_word = ""
    for word, index in tokenizer.word_index.items():
        if index == predicted:
            output_word = word
            break
    seed_text += " " + output_word
print(seed_text)

"""
path = "../data/glove.6B.100d.txt"
if not os.path.isfile(path):
    wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/sonnets.txt", path, bar=bar_progress)
    
embeddings_index = {};
with open(path) as f:
    for line in f:
        values = line.split();
        word = values[0];
        coefs = np.asarray(values[1:], dtype='float32');
        embeddings_index[word] = coefs;

embeddings_matrix = np.zeros((vocab_size+1, embedding_dim));
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word);
    if embedding_vector is not None:
        embeddings_matrix[i] = embedding_vector;
        
# tf.keras.layers.Embedding(vocab_size+1, embedding_dim, input_length=max_length, weights=[embeddings_matrix], trainable=False),
"""

