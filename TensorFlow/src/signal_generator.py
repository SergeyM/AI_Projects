import numpy as np


class SignalGenerator:
    # Trend
    @staticmethod
    def trend(time, slope=0.):
        return slope * time

    # Seasonality
    @staticmethod
    def seasonal_pattern(season_time):
        return np.where(season_time < 0.4,
                        np.cos(season_time * 2 * np.pi),
                        1 / np.exp(3 * season_time))

    @staticmethod
    def seasonality(time, period, amplitude=1, phase=0):
        season_time = ((time + phase) % period) / period
        return amplitude * SignalGenerator.seasonal_pattern(season_time)

    # Noise
    @staticmethod
    def white_noise(time, noise_level=1, seed=None):
        rnd = np.random.RandomState(seed)
        return rnd.randn(len(time)) * noise_level

    # Auto Correlation
    @staticmethod
    def autocorrelation0(time, amplitude, seed=None):
        rnd = np.random.RandomState(seed)
        phi1 = 0.5
        phi2 = -0.1
        ar = rnd.randn(len(time) + 50)
        ar[:50] = 100
        for step in range(50, len(time) + 50):
            ar[step] += phi1 * ar[step - 50]
            ar[step] += phi2 * ar[step - 33]
        return ar[50:] * amplitude

    @staticmethod
    def autocorrelation1(time, amplitude, seed=None):
        rnd = np.random.RandomState(seed)
        phi = 0.8
        ar = rnd.randn(len(time) + 1)
        for step in range(1, len(time) + 1):
            ar[step] += phi * ar[step - 1]
        return ar[1:] * amplitude

    @staticmethod
    def autocorrelation2(source, phis):
        ar = source.copy()
        for step, value in enumerate(source):
            for lag, phi in phis.items():
                if step - lag > 0:
                    ar[step] += phi * ar[step - lag]
        return ar

    # Impulses
    @staticmethod
    def impulses(time, num_impulses=10, amplitude=1, seed=None):
        rnd = np.random.RandomState(seed)
        impulse_indices = rnd.randint(len(time), size=num_impulses)
        series = np.zeros(len(time))
        for index in impulse_indices:
            series[index] += rnd.rand() * amplitude
        return series
