import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import wget
import sys
import os
import zipfile

from utils import bar_progress, plot_history
from callbacks import CustomCallback

print(tf.__version__)
np.set_printoptions(linewidth=200)

# ---------------------------- DATA ----------------------------

""" Dataset 1: Linear Data
training_data = np.array([-1.0,  0.0, 1.0, 2.0, 3.0, 4.0], dtype=float)
training_labels = np.array([-3.0, -1.0, 1.0, 3.0, 5.0, 7.0], dtype=float)
"""

""" Dataset 2: MNIST Fashion Dataset 
mnist = tf.keras.datasets.fashion_mnist
(training_images, training_labels), (test_images, test_labels) = mnist.load_data()
# plt.imshow(training_images[0])
# print(training_labels[0])
# print(training_images[0])

# Reshape Tensor for CNN
training_images = training_images.reshape(len(training_images), 28, 28, 1)
test_images = test_images.reshape(len(test_images), 28, 28, 1)

# Normalization
training_images = training_images / 255.0
test_images = test_images / 255.0
"""

url_training = "https://storage.googleapis.com/laurencemoroney-blog.appspot.com/horse-or-human.zip"
url_validation = "https://storage.googleapis.com/laurencemoroney-blog.appspot.com/validation-horse-or-human.zip"
path_training = "../data/horse-or-human.zip"
path_validation = "../data/validation-horse-or-human.zip"
if not os.path.isfile(path_training):
    wget.download(url_training, path_training, bar=bar_progress)
if not os.path.isfile(path_validation):
    wget.download(url_validation, path_validation, bar=bar_progress)
with zipfile.ZipFile(path_training, 'r') as zip_ref:
    zip_ref.extractall('../data/horse-or-human')
with zipfile.ZipFile(path_validation, 'r') as zip_ref:
    zip_ref.extractall('../data/validation-horse-or-human')

print()
train_horse_dir = os.path.join('../data/horse-or-human/horses')
train_human_dir = os.path.join('../data/horse-or-human/humans')
validation_horse_dir = os.path.join('../data/validation-horse-or-human/horses')
validation_human_dir = os.path.join('../data/validation-horse-or-human/humans')
print('total training horse images:', len(os.listdir(train_horse_dir)))
print('total training human images:', len(os.listdir(train_human_dir)))
print('total validation horse images:', len(os.listdir(validation_horse_dir)))
print('total validation human images:', len(os.listdir(validation_human_dir)))

# nrows = 4
# ncols = 4
# pic_index = 0
#
# fig = plt.gcf()
# fig.set_size_inches(ncols * 4, nrows * 4)
#
# pic_index += 8
# next_horse_pix = [os.path.join(train_horse_dir, fname) for fname in train_horse_names[pic_index-8:pic_index]]
# next_human_pix = [os.path.join(train_human_dir, fname) for fname in train_human_names[pic_index-8:pic_index]]
# for i, img_path in enumerate(next_horse_pix+next_human_pix):
#     # Set up subplot; subplot indices start at 1
#     sp = plt.subplot(nrows, ncols, i + 1)
#     sp.axis('Off')  # Don't show axes (or gridlines)
#     img = mpimg.imread(img_path)
#     plt.imshow(img)
# plt.show()

datagen = ImageDataGenerator(rescale=1/255)
train_generator = datagen.flow_from_directory(
    '../data/horse-or-human/',  # This is the source directory for training images
    target_size=(150, 150),  # All images will be resized to 150x150
    batch_size=128,
    class_mode='binary'
)
validation_generator = datagen.flow_from_directory(
    '../data/validation-horse-or-human/',
    target_size=(150, 150),
    batch_size=32,
    class_mode='binary'
)


# ---------------------------- MODEL ----------------------------

""" Model 1: Linear Model
model = tf.keras.Sequential([tf.keras.layers.Dense(units=1, input_shape=[1])])
"""

""" Model 2: Fully Connected Layers
model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation=tf.nn.relu),
    tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])
"""

""" Model 3: First CNN
model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=(28, 28, 1)),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(10, activation='softmax')
])
"""

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(16, (3, 3), activation='relu', input_shape=(150, 150, 3)),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(32, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(512, activation='relu'),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

model.summary()

# ---------------------------- COMPILE ----------------------------

# model.compile(optimizer='sgd', loss='mean_squared_error')

# model.compile(
#     optimizer='adam',
#     loss='sparse_categorical_crossentropy',
#     metrics=['accuracy']
# )

from tensorflow.keras.optimizers import RMSprop
model.compile(
    optimizer=RMSprop(lr=0.001),
    loss='binary_crossentropy',
    metrics=['accuracy']
)

# ---------------------------- FIT, EVALUATE, PREDICT ----------------------------

# history = model.fit(training_images, training_labels, epochs=20, callbacks=[CustomCallback()])
history = model.fit(
    train_generator,
    steps_per_epoch=8,
    epochs=15,
    verbose=1,
    callbacks=[CustomCallback()],
    validation_data=validation_generator,
    validation_steps=8
)

# test_loss = model.evaluate(test_images, test_labels)
# classifications = model.predict(test_images)


# ---------------------------- EXTRA ----------------------------

plot_history(history)

"""
import numpy as np
from google.colab import files
from keras.preprocessing import image

uploaded = files.upload()

for fn in uploaded.keys():
 
  # predicting images
  path = '/content/' + fn
  img = image.load_img(path, target_size=(300, 300))
  x = image.img_to_array(img)
  x = np.expand_dims(x, axis=0)

  images = np.vstack([x])
  classes = model.predict(images, batch_size=10)
  print(classes[0])
  if classes[0]>0.5:
    print(fn + " is a human")
  else:
    print(fn + " is a horse")
"""
