import sys
import matplotlib.pyplot as plt


def bar_progress(current, total, width=80):
    progress_message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
    # Don't use print() as it will print in new line every time.
    sys.stdout.write("\r" + progress_message)
    sys.stdout.flush()


def plot_history(history, start=0, end=None):
    # TODO if no validation set is given
    epochs = range(len(history.history['loss'][start:end]))  # Get number of epochs

    if "accuracy" in history.history:
        plt.plot(epochs, history.history['accuracy'][start:end])
        if 'val_accuracy' in history.history:
            plt.plot(epochs, history.history['val_accuracy'][start:end])
        plt.xlabel("Epochs")
        plt.ylabel("Accuracy")
        plt.title('Training and validation accuracy')
        plt.figure()

    plt.plot(epochs, history.history['loss'][start:end])
    if 'val_loss' in history.history:
        plt.plot(epochs, history.history['val_loss'][start:end])
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title('Training and validation loss')
    plt.show()


def plot_series(time, series, format_="-", start=0, end=None, label=None):
    plt.plot(time[start:end], series[start:end], format_, label=label)
    plt.xlabel("Time")
    plt.ylabel("Value")
    if label:
        plt.legend(fontsize=14)
    plt.grid(True)
