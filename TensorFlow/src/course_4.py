import csv

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import wget
import sys
import os
import zipfile
import json
import io

from utils import bar_progress, plot_history, plot_series
from callbacks import CustomCallback
from signal_generator import SignalGenerator

print(tf.__version__)
np.set_printoptions(linewidth=200)

"""
Univariate Time Series
Multivariate Time Series

Non-stationary Time Series

Trend, Seasonality, Autocorrelation, Noise
"""

# ---------------------------- HYPER PARAMETERS ----------------------------


# ---------------------------- DATA ----------------------------

# Data 1
time = np.arange(4 * 365 + 1, dtype="float32")
baseline = 10
amplitude = 40
slope = 0.05
noise_level = 5
moving_average_window_size = 30

series = baseline + SignalGenerator.trend(time, slope)
series += SignalGenerator.seasonality(time, period=365, amplitude=amplitude)
series += SignalGenerator.white_noise(time, noise_level, seed=42)

plt.figure(figsize=(10, 6))
plot_series(time, series)
plt.show()

split_time = 1000
time_train = time[:split_time]
x_train = series[:split_time]
time_valid = time[split_time:]
x_valid = series[split_time:]


# Data 2

def windowed_dataset(series, window_size, batch_size, shuffle_buffer_size, verbose=False):
    dataset = tf.expand_dims(series, axis=-1)
    dataset = tf.data.Dataset.from_tensor_slices(dataset)
    dataset = dataset.window(window_size + 1, shift=1, drop_remainder=True)
    dataset = dataset.flat_map(lambda window: window.batch(window_size + 1))
    # Shuffle to remove sequence bias
    # Sequence bias is when the order of things can impact the selection of things
    dataset = dataset.shuffle(buffer_size=shuffle_buffer_size)
    dataset = dataset.map(lambda window: (window[:-1], window[1:]))  # (lambda w: (w[:-1], w[-1]))
    dataset = dataset.batch(batch_size).prefetch(1)
    if verbose:
        for x, y in dataset:
            print(x.numpy(), y.numpy())
    return dataset


# window_size = 20
# batch_size = 32
# shuffle_buffer_size = 1000
# dataset = windowed_dataset(x_train, window_size, batch_size, shuffle_buffer_size)

# Data 3

path = "../data/Sunspots.csv"
if not os.path.isfile(path):
    wget.download("https://storage.googleapis.com/laurencemoroney-blog.appspot.com/Sunspots.csv", path,
                  bar=bar_progress)

time_step = []
sunspots = []
with open(path) as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    next(reader)
    for row in reader:
        sunspots.append(float(row[2]))
        time_step.append(int(row[0]))

sunspot_series = np.array(sunspots)
sunspot_time = np.array(time_step)

plt.figure(figsize=(10, 6))
plot_series(sunspot_time, sunspot_series)
plt.show()

split_time = 3000
time_train = sunspot_time[:split_time]
x_train = sunspot_series[:split_time]
time_valid = sunspot_time[split_time:]
x_valid = sunspot_series[split_time:]

window_size = 60
batch_size = 100
shuffle_buffer_size = 1000
dataset = windowed_dataset(x_train, window_size, batch_size, shuffle_buffer_size)

# ---------------------------- NON-ML APPROACH (TIME SERIES ANALYZES) ----------------------------

"""
# Naive Forecast (lags 1 step behind)
naive_forecast = series[split_time - 1:-1]
plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid)
plot_series(time_valid, naive_forecast)
plt.show()

# Zoomed in
plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid, start=0, end=150)
plot_series(time_valid, naive_forecast, start=1, end=151)
plt.show()

print("Performance Baseline (Naive Forecast with lag=1)")
print("MSE:", tf.keras.metrics.mean_squared_error(x_valid, naive_forecast).numpy())
print("MAE:", tf.keras.metrics.mean_absolute_error(x_valid, naive_forecast).numpy())


# Moving Average
def moving_average_forecast(series, window_size):
    forecast = []
    for time in range(len(series) - window_size):
        forecast.append(series[time:time + window_size].mean())
    return np.array(forecast)


moving_avg = moving_average_forecast(series, moving_average_window_size)[split_time - moving_average_window_size:]

plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid)
plot_series(time_valid, moving_avg)
plt.show()

print("Moving Average")
print("MSE:", tf.keras.metrics.mean_squared_error(x_valid, moving_avg).numpy())
print("MAE:", tf.keras.metrics.mean_absolute_error(x_valid, moving_avg).numpy())

# Differencing
# Since the seasonality period is 365 days, we will subtract the value at time t – 365 from the value at time t
diff_series = (series[365:] - series[:-365])
diff_time = time[365:]
diff_moving_avg = moving_average_forecast(diff_series, 50)[split_time - 365 - 50:]  # window_size = 50
diff_moving_avg_plus_past = series[split_time - 365:-365] + diff_moving_avg

plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid)
plot_series(time_valid, diff_moving_avg_plus_past)
plt.show()

print("Moving Average + Differencing")
print("MSE:", tf.keras.metrics.mean_squared_error(x_valid, diff_moving_avg_plus_past).numpy())
print("MAE:", tf.keras.metrics.mean_absolute_error(x_valid, diff_moving_avg_plus_past).numpy())

# use a moving averaging on past values to remove some of the noise
diff_moving_avg_plus_smooth_past = moving_average_forecast(series[split_time - 370:-360], 10) + diff_moving_avg

plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid)
plot_series(time_valid, diff_moving_avg_plus_smooth_past)
plt.show()

print("Moving Average + Smooth Past (Moving Average of past values)")
print("MSE:", tf.keras.metrics.mean_squared_error(x_valid, diff_moving_avg_plus_smooth_past).numpy())
print("MAE:", tf.keras.metrics.mean_absolute_error(x_valid, diff_moving_avg_plus_smooth_past).numpy())
"""

# ---------------------------- MODEL ----------------------------

tf.keras.backend.clear_session()

# model = tf.keras.Sequential([
#     tf.keras.layers.Dense(10, input_shape=[window_size], activation="relu"),
#     tf.keras.layers.Dense(10, activation="relu"),
#     tf.keras.layers.Dense(1)
# ])

# model = tf.keras.Sequential([
#     tf.keras.layers.Lambda(lambda x: tf.expand_dims(x, axis=-1), input_shape=[None]),
#     tf.keras.layers.SimpleRNN(40, return_sequences=True),
#     tf.keras.layers.SimpleRNN(40),
#     tf.keras.layers.Dense(1),
#     tf.keras.layers.Lambda(lambda x: x * 100.0)
# ])

# model = tf.keras.Sequential([
#     tf.keras.layers.Lambda(lambda x: tf.expand_dims(x, axis=-1), input_shape=[None]),
#     tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32, return_sequences=True)),
#     tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32)),
#     tf.keras.layers.Dense(1),
#     tf.keras.layers.Lambda(lambda x: x * 100.0)
# ])

# model = tf.keras.Sequential([
#     tf.keras.layers.Conv1D(filters=32, kernel_size=5, strides=1, padding="causal", activation="relu",
#                            input_shape=[None, 1]),
#     tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32, return_sequences=True)),
#     tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(32, return_sequences=True)),
#     tf.keras.layers.Dense(1),
#     tf.keras.layers.Lambda(lambda x: x * 200)
# ])

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv1D(filters=60, kernel_size=5, strides=1, padding="causal", activation="relu",
                           input_shape=[None, 1]),
    tf.keras.layers.LSTM(60, return_sequences=True),
    tf.keras.layers.LSTM(60, return_sequences=True),
    tf.keras.layers.Dense(30, activation="relu"),
    tf.keras.layers.Dense(10, activation="relu"),
    tf.keras.layers.Dense(1),
    tf.keras.layers.Lambda(lambda x: x * 400)
])

# ---------------------------- COMPILE ----------------------------

# lr_schedule = tf.keras.callbacks.LearningRateScheduler(
#     lambda epoch: 1e-8 * (10 ** (epoch/20))
# )
# optimizer = tf.keras.optimizers.SGD(lr=1e-8, momentum=0.9)

optimizer = tf.keras.optimizers.SGD(lr=1e-5, momentum=0.9)

# model.compile(optimizer=optimizer, loss="mse")

model.compile(loss=tf.keras.losses.Huber(), optimizer=optimizer, metrics=["mae"])

# ---------------------------- FIT, EVALUATE, PREDICT ----------------------------

# history = model.fit(dataset, epochs=100, verbose=1, callbacks=[lr_schedule])

history = model.fit(dataset, epochs=500)


# ---------------------------- EXTRA ----------------------------

# Finding optimal learning rate
# learning_rates = 1e-8 * (10 ** (np.arange(100) / 20))
# plt.semilogx(learning_rates, history.history["loss"])
# plt.axis([1e-8, 1e-3, 0, 300])
# plt.show()
# exit()

# Forecast

# forecast = list()
# for time in range(len(series) - window_size):
#     forecast.append(model.predict(series[time:time + window_size][np.newaxis]))
# forecast = forecast[split_time - window_size:]
# results = np.array(forecast)[:, 0, 0]

def model_forecast(model, series, window_size):
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size))
    ds = ds.batch(32).prefetch(1)
    forecast = model.predict(ds)
    return forecast


results = model_forecast(model, series[..., np.newaxis], window_size)
results = results[split_time - window_size:-1, -1, 0]

plt.figure(figsize=(10, 6))
plot_series(time_valid, x_valid)
plot_series(time_valid, results)
plt.show()

print("DNN Forecast")
print("MSE:", tf.keras.metrics.mean_squared_error(x_valid, results).numpy())
print("MAE:", tf.keras.metrics.mean_absolute_error(x_valid, results).numpy())

plot_history(history)

# signal = SignalGenerator.impulses(time, 10, seed=42)
# series = SignalGenerator.autocorrelation2(signal, {1: 0.70, 50: 0.2})
#
# plot_series(time, series)
# plt.plot(time, signal, "k-")
# plt.show()
