Pipelines = Sequence of data processing components

1. Look at the Big Picture
    1. Frame the Problem
    2. Select a Performance Measure
    3. Check the Assumptions
2. Get the Data
    1. Create the Workspace
    2. Download/Fetch the Data
    3. Understand the data (plot histograms, check datatype, etc.)
    4. Create a Test Set (prevent data snooping bias)
        - random sampling (can introduce sample bias)
        - stratified sampling: divide data into homogeneous subsets called strata, sample right number of instances from each stratum to guarantee representative test set
3. Explore the Data (Discover and Visualize the Data)
    - Visualize Data
    - Looking for Correlations
    - Finding data quirks (might clean them up)
    - Check attribute combinations, and the correlation to the target value
4. Prepare the Data
    - write transformation functions
    - Data Cleaning
        - missing numerical features (options)
            1. Get rid of the corresponding rows with missing features
            2. Get rid of whole attribute (column)
            3 Set the values to some value (zero, mean, median, ...)
        - transform categorical data to numbers
            - ordinal encoding
            - one hot encoding (in case of a large number of categories, replace categories with useful numerical features or replace category with a learnable, low-dimensional vector (embedding) => representation learning)
        - custom transformers
            - scikit: implement fit(), transform(), fit_transform()
    - Feature Scaling
        - Note: Scaling the target values is generally not required
        - min-max scaling (normalization): => values in \[0, 1\] - (x - min)/(max - min) - scikit MinMaxScaler 
        - standardization: => (x - mean) / standard deviation - scikit StandardScaler
    - Using Transformation Pipelines
5. Select and Train a Model
    - Training and Evaluating on the Training Set
    - Underfitting:
        - Features don't provide enough information
        - Model is not powerful enough
    - Overfitting
    - Better Evaluation Using Cross-Validation 
    - Try many different models without spending too much time tweaking the hyperparameters => shortlist few promosing models
    - save models: hyperparameters, trained parameters, cross-validation scores, (actual predictions)
6. Fine-Tune Your Model
    - Grid Search
    - Randomized Search
    - Ensemble Methods
    - Analyze the Best Model and Their Errors
    - Evaluate Your System on the Test Set
5. Present the Solution
6. Launch, Monitor, and Maintain Your System 
    - polish code, write doc and tests, ...
    - deploy (e.g. as a Web Service + REST API or on the cloud (Google Cloud AI Platform))
    - write monitoring code (check system's live performance + trigger alerts when performance drops)
        - could be inferred from downstream metrics
        - human analysis
    - Retrain models (Automatically?)
        - Collect fresh data regularly and label it
        - Write script to train the model and fine tune the hyperparameters automatically
        - Write another script that will evaluate both the new and previous model on the updated test set, and deploy if performance hasn't decreased
    - Keep backups of every model you create (for roll back)
    - Keep backups of every version of the datasets    
    
___

Performance Measure for Regression: Root Mean Square Error (RMSE)  
RMSE(\mathbf{X}, h) = \sqrt{\frac{1}{m} \sum_{i=1}^{m} (h(\mathbf{x}^{(i)}) - y^{(i)})^{2}}

If many outliers: Mean Absolute Error (MAE)
MAE(\mathbf{X}, h) = \frac{1}{m} \sum_{i=1}^{m} | h(\mathbf{x}^{(i)}) - y^{(i)} |

Various norms are possible:
- Euclidean norm or l_{2} norm
- Manhattan norm or l_{1} norm
- l_{k} norm: \left \| v \right \|_{k} = (|v_{0}|^{k} + ... +|v_{n}|^{k})^{\frac{1}{k}}
    - the higher the norm index, the more it focuses on large values and neglects small ones

Standard Deviation: Gaussian/Normal Distribution -> "68-95-99.7" rule: 68% of values fall within 1\*sigma of the mean, 95% within 2\*sigma, 99.7% within 3\*sigma  
Percentile: indicates the value below which a given percentage of observations in a group of observations fall
    25% = 25th percentile (first quartile); 50% = median; 75% = 75th percentile (third quartile) 


Ensemble Learning: Building model on top of many other models (e.g. Random Forest)
