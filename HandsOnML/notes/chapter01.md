**“A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E.” - Tom Mitchell**

Machine Learning is great for:
- Problems for which existing solutions require a lot of hand-tuning or long lists of rules: one Machine Learning algorithm can often simplify code and perform better.
- Complex problems for which there is no good solution at all using a traditional approach: the best Machine Learning techniques can find a solution.
- Fluctuating environments: a Machine Learning system can adapt to new data.
- Getting insights about complex problems and large amounts of data.

## Types of Machine Learning Systems

- supervised, unsupervised, semi-supervised, reinforcement learning
    - supervised: labeled data (e.g. classification, regression)
    - unsupervised: unlabeled data (e.g. clustering, visualization, dimensionality reduction, anomaly/novelty detection, association rule learning)
    - semi-supervised: partially labeled
    - reinforcement: agent (learning system) -> observe environment -> select & perform actions -> get rewards/penalties => learn the best strategy (policy)
- online, batch learning
    - batch learning (offline): incapable of learning incrementally 
    - online learning: train system incrementally by feeding it data instances sequentially (mini-batches)
        - for continuous flow data or limited computing resources
        - out-of-core learning (data doesn't fit in memory)
- instance-based, model-based learning
    - instance-based learning (learning by heart + generalizing using similarity measures)
    - model-based learning (build a model out of examples)
        1. data acquisition
        2. model selection
        3. specify a performance measure (fitness function (how good) or cost function (how bad))
        4. training the model (finding the model parameters that will make it best fit the training data)
        
feature
attribute
feature extraction

good idea to run dimensionality reduction before feeding into other ML algorithm

## Main Challenges of Machine Learning

### BAD DATA

#### Insufficient Quantity of Training Data

- [Paper: importance of data](https://homl.info/6)
- ["The Unreasonable Effectiveness of Data"](https://homl.info/7) 
- data > algorithm 

#### Non-Representative Training Data

Crucial to use a training set that is representative of the cases you want to generalize to.

#### Poor-Quality Data

- full of errors, outliers, and noise
=> clean up training data
    - discard clear outliers
    - some instances are missing features -> ignore attribute | ignore instances | fill in missing values

#### Irrelevant Features

- Garbage in, Garbage out
- Feature Engineering: 
    - Feature selection
    - Feature extraction
    - Creating new features by gathering new data

### BAD ALGORITHMS

#### Overfitting the Training Data

- Overgeneralizing
- performs well on training data, but doesn't generalize
- model is too complex relative to the amount nad noisiness of the training data
- Solutions:
    - simplify the model (fewer parameters; reducing number of attributes in the data; constraining the model)
    - gather more training data
    - reduce the noise in the training data 
- Constraining the model = Regularization
- Hyperparameter = parameter of a learning algorithm

#### Underfitting the Training Data

- model too simple to learn the underlying structure of the data
- Solutions:
    - select more powerful model (more parameters)
    - feed better features to the learning algorithm 
    - reduce the constraints on the model
    
## Testing and Validating

- split data in training set and test set (typically 80%/20% split)
- error rate in new cases = generalization error
- evaluating model in test set -> estimation of generalization error

### Hyperparameter Tuning and Model Selection

- holdout validation => holdout a validation set from the training set
- tune hyperparameters using the validation set
- cross-validation (each model is evaluated once per validation set after it is trained on the rest of the training data)

### Data Mismatch

- data must be as representative as possible
- Check by using train-dev set:
    - After training (on trading set without train-dev set) evaluate on train-dev set
    - If performs poorly on train-dev set => data mismatch
    - Possible solution: preprocess data

___

[No Free Lunch Theorem](https://homl.info/8)
- without assumptions there is no reason to prefer one model over any other

___

## Exercises

1. How would you define Machine Learning?: 
    - Building system which learns based on training data to perform better at some task, given some performance measure.

2. Can you name four types of problems where it shines?:
    - complex problems without algorithmic solution
    - replace long lists of hand-tuned rules
    - systems that adapt
    - help humans to learn 

3. What is a labeled training set?:
    - Data with attributes and desired solution (labels) for each instance

4. What are the two most common supervised tasks?:
    - Classification & Regression

5. Can you name four common unsupervised tasks?:
    - Clustering, Dimensionality Reduction, Visualization, Association Rule Learning 

6. What types of Machine Learning algorithm would you use to allow a robot to walk in various unknown terrains?:
    - Reinforcement learning

7. What type of algorithm would you use to segment your customers into multiple groups?:
    - supervised or unsupervised

8. Would you frame the problem of spam detection as a supervised learning problem or an unsupervised learning problem?:
    - supervised

9. What is an online learning system?:
    - Learns incrementally
    - The data is provided in mini-batches or sequentially

10. What is out-of-core learning?:
    - Learning where not the whole data is stored in main memory, but is loaded and discarded sequentially into main memory (mini-batches)

11. What type if learning algorithm relies on a similarity measure to make predictions?:
    - Instance-based algorithms

12. What is the difference between a model parameter and a learning algorithm's hyperparameter?:
    - Hyperparameter is set before the model starts to learn
    - Model parameter is adjusted during training based on the training data and the outcome

13. What do model-based learning algorithms search for? What is the most common strategy they use to succeed? How do they make predictions?:
    - searches for parameters which fits the training data the best (minimizes the cost function or maximizes the fitness function)
    - Most common strategy is training by minimizing a cost function
    - Predictions are made by using the learned model parameters with the new instance features

14. Can you name four of the main challenges in Machine Learning?:
    - Irrelevant data; Overfitting and Underfitting; Not enough data; poor quality data

15. If your model performs great on the training data but generalizes poorly to new instances, what is happening? Can you name three possible solutions?:
    - Overfitting
    - Solutions: Regularization, using simpler model, more training data; reducing noise in training data

16. What is a test set, and why would you want to use it?:
    - Check how the model performs on unseen data
    - Estimate the generalization error

17. What is the purpose of a validation set?:
    - To tune the hyperparameters

18. What is the train-dev set, when do you need it, and how do you use it?:
    - in case of potential data mismatch
    - train model on training data without train-dev set -> evaluate on train-dev set -> if model performs well on both (train-dev and training) there is a data mismatch between training set and validation set

19. What can go wrong if you tune hyperparameters using the test set?:
    - Choose best hyperparameters for specific test set; model performs bad in production
