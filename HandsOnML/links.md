### Popular open data repositories
- [UC Irvine Machine Learning Repository](http://archive.isc.uci.edu/ml/)
- [Kaggle datasets](https://www.kaggle.com/datasets)
- [Amazon's AWS datasets](https://registry.opendata.aws/)

### Meta portals (list open data repositories)
- [Data Portals](http://dataportals.org/)
- [OpenDataMonitor](http://opendatamonitor.eu/)
- [Quandl](http://quandl.com/)

### Other pages listing many popular open data repositories
- [Wikipedia's list of ML datasets](https://homl.info/9)
- [Quora](https://homl.info/10)
- [Dataset Subreddit](https://www.reddit.com/r/datasets)

