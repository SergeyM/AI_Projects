import os
import tarfile
import urllib.request
import sys
import wget
import pandas


def bar_progress(current, total, width=80):
    progress_message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
    # Don't use print() as it will print in new line every time.
    sys.stdout.write("\r" + progress_message)
    sys.stdout.flush()


def fetch_data(url, path, name):
    os.makedirs(path, exist_ok=True)
    file_path = os.path.join(path, name)
    # urllib.request.urlretrieve(url, file_path)
    if not os.path.isfile(file_path):
        wget.download(url, file_path, bar=bar_progress)
    if file_path.endswith(".tgz"):
        with tarfile.open(file_path) as tgz:
            tgz.extractall(path=path)


def load_csv(path):
    return pandas.read_csv(path)
