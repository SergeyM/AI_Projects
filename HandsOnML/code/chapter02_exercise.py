import os

import numpy as np
from scipy.stats import loguniform, reciprocal, expon
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV, RandomizedSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.svm import SVR
import pandas as pd

import utils

DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"
HOUSING_PATH = os.path.join("..", "data", "housing")
HOUSING_FILE_NAME = "housing"

utils.fetch_data(HOUSING_URL, HOUSING_PATH, HOUSING_FILE_NAME + ".tgz")
housing = utils.load_csv(os.path.join(HOUSING_PATH, HOUSING_FILE_NAME + ".csv"))

housing["income_category"] = pd.cut(housing["median_income"],
                                    bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
                                    labels=[1, 2, 3, 4, 5])
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
train_index, test_index = next(split.split(housing, housing["income_category"]))
strat_train_set = housing.loc[train_index]
strat_test_set = housing.loc[test_index]

housing = strat_train_set.drop("median_house_value", axis=1)
housing_labels = strat_train_set["median_house_value"].copy()


class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
    col_names = "total_rooms", "total_bedrooms", "population", "households"

    def __init__(self, data, add_bedrooms_per_room=True):  # no *args or **kwargs
        self.data = data
        self.room_ix, self.bedrooms_ix, self.population_ix, self.households_ix = [
            self.data.columns.get_loc(c) for c in self.col_names
        ]
        self.add_bedrooms_per_room = add_bedrooms_per_room

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        rooms_per_household = X[:, self.room_ix] / X[:, self.households_ix]
        population_per_household = X[:, self.population_ix] / X[:, self.households_ix]
        if self.add_bedrooms_per_room:
            bedrooms_per_room = X[:, self.bedrooms_ix] / X[:, self.room_ix]
            return np.c_[X, rooms_per_household, population_per_household, bedrooms_per_room]
        else:
            return np.c_[X, rooms_per_household, population_per_household]


housing_num = housing.drop("ocean_proximity", axis=1)
num_attribs = list(housing_num)
cat_attribs = ["ocean_proximity"]

num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="mean")),
    ('attribs_adder', CombinedAttributesAdder(housing_num)),
    ('std_scaler', StandardScaler())
])

full_pipeline = ColumnTransformer([
    ("num", num_pipeline, num_attribs),
    ("cat", OneHotEncoder(), cat_attribs)
])

housing_prepared = full_pipeline.fit_transform(housing)

params_grid = [
    {"kernel": ["linear"], "C": [0.01, 0.1, 1, 10, 100, 1000]},
    {"kernel": ["rbf"], "C": [0.01, 0.1, 1, 10, 100, 1000], "gamma": ["scale", "auto", 0.01, 0.1, 1.0]},
]
svr = SVR()
grid_search = GridSearchCV(svr, params_grid, scoring="neg_mean_squared_error", cv=5, verbose=2, return_train_score=True)
grid_search.fit(housing_prepared, housing_labels)

print(grid_search.best_score_)
print(grid_search.best_params_)

param_distribs = {
    'kernel': ['linear', 'rbf'],
    'C': reciprocal(20, 200000),
    'gamma': expon(scale=1.0),
}

svm_reg = SVR()
rnd_search = RandomizedSearchCV(svm_reg, param_distributions=param_distribs,
                                n_iter=50, cv=5, scoring='neg_mean_squared_error',
                                verbose=2, n_jobs=-1)
rnd_search.fit(housing_prepared, housing_labels)

print(rnd_search.best_score_)
print(rnd_search.best_params_)
