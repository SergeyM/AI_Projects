import numpy as np
from sklearn.datasets import fetch_openml
import matplotlib as mpl
import matplotlib.pyplot as plt
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, precision_recall_curve, \
    roc_curve, roc_auc_score
from sklearn.preprocessing import StandardScaler

mnist = fetch_openml('mnist_784', version=1)
print(mnist.keys())
# DESCR = description of the dataset
# data = array with row per instance and column per feature
# target = array of label

X, y = mnist["data"], mnist["target"].astype(np.unit8)
print("X:", X.shape, "y:", y.shape)

plt.imshow(X[0].reshape(28, 28), cmap="binary")
plt.axis("off")
plt.show()

split = 60000
X_train, X_test, y_train, y_test = X[:split], X[split:], y[:split], y[split:]

# Binary Classification

y_train_5 = (y_train == 5)
y_test_5 = (y_test == 5)

# Stochastic Gradient Descent Classifier

sgd_clf = SGDClassifier()  # random_state=42 for reproducible results
sgd_clf.fit(X_train, y_train_5)
# sgd_clf.predict()

# PERFORMANCE MEASURE

# Measuring Accuracy Using Cross-Validation

"""
Custom Cross-Validation Implementation

from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone

skfolds = StratifiedKFold(n_splits=3, shuffle=True)
for train_index, test_index in skfolds.split(X_train, y_train_5):
    clone_clf = clone(sgd_clf)
    X_train_folds = X_train[train_index]
    y_train_folds = y_train_5[train_index]
    X_test_folds = X_train[test_index]
    y_test_folds = y_train_5[test_index]
    
    clone_clf.fit(X_train_folds, y_train_folds)
    y_pred = clone_clf.predict(X_test_folds)
    n_correct = sum(y_pred == y_test_folds)
    print(n_correct / len(y_pred))
    
"""

print(cross_val_score(sgd_clf, X_train, y_train_5, cv=3, scoring="accuracy"))

# Accuracy is generally not the preferred performance measure for classifiers (especially with skewed datasets where classes are much more frequent than others)
# e.g. accuracy of a dumb classifier who guesses always not 5 is at 90% (because 90% of data is not 5)

# Confusion Matrix

y_train_predict = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3)
print(confusion_matrix(y_train_5, y_train_predict))
print("Perfect matrix: \n", confusion_matrix(y_train_5, y_train_5))

# Precision: accuracy of the positive predictions; TP / (TP + FP)
# Recall/Sensitivity/True Positive Rate (TPR): ratio pf positive instances correctly classified; TP / (TP + FN)
# F1 score: harmonic mean of precision and recall; 2 * (precision * recall) / (precision + recall)
# choose score depending on context

print(precision_score(y_train_5, y_train_predict))
print(recall_score(y_train_5, y_train_predict))
print(f1_score(y_train_5, y_train_predict))

# Precision/Recall Trade-off
# by moving the threshold (decision function) either the precision increases and recall decreases other the other way arround

y_scores = cross_val_predict(sgd_clf, X_train, y_train_5, cv=3, method="decision_function")
precisions, recalls, thresholds = precision_recall_curve(y_train_5, y_scores)
plt.plot(thresholds, precisions[:-1], "b--", lable="Precision")
plt.plot(thresholds, recalls[:-1], "g-", lable="Recall")
plt.show()

# TODO plot precision (y) recall (x) diagram

threshold_90_precision = thresholds[np.argmax(precisions >= 0.9)]
y_train_pred_90 = (y_scores >= threshold_90_precision)
print(precision_score(y_train_5, y_train_pred_90))
print(recall_score(y_train_5, y_train_pred_90))

# ROC Curve (receiver operating characteristic)
# for binary classifiers
# plot recall (true positive rate) against false positive rate; sensitivity (recall) vs. 1 - specificity
# False Positive Rate: ratio of negative instances that are incorrectly classified as positive; 1 - TNR (true negative rate/specificity)
# True Negative Rate/Specificity: ration of negative instances that are correctly classified as negative
# TPR/FPR Trade-off

fpr, tpr, thresholds = roc_curve(y_train_5, y_scores)
plt.plot(fpr, tpr, linewidth=2)
plt.plot([0, 1], [0, 1], "k--")  # ROC curve of a purely random classifier
plt.show()

# Compare classifiers by measuring the area under the curve (AUC)
print(roc_auc_score(y_train_5, y_scores))

# Use precision/recall curve metric if the positive class is rare or when you care more about the false positives that the false negatives
# Use ROC curve otherwise

forest_clf = RandomForestClassifier()
y_probas_forest = cross_val_predict(forest_clf, X_train, y_train_5, cv=3, method="predict_proba")
y_scores_forest = y_probas_forest[:, 1]  # score = probability of positive class

fpr_forest, tpr_forest, thresholds_forest = roc_curve(y_train_5, y_scores_forest)
plt.plot(fpr, tpr, "b:", label="SGD")
plt.plot(fpr_forest, tpr_forest, label="Random Forest")
plt.plot([0, 1], [0, 1], "k--")  # ROC curve of a purely random classifier
plt.legend(loc="lower right")
plt.show()

print(roc_auc_score(y_train_5, y_scores_forest))

# TODO calculate precision and recall for random forest classifier

# Multiclass Classification

# binary classifier to multiclass classifier
# -> one-versus-the-rest (OvR) strategy (train classifier for each class and take highest score as final answer)
# -> one-versus-one (OvO) strategy (train classifier for every pair of classes => N*(N-1)/2 classifiers
#   some algorithms scale poorly on to large datasets => use OvO strategy

svm_clf = SVC()
svm_clf.fit(X_train, y_train)

ovo_clf = OneVsOneClassifier(SVC())
ovo_clf.fit(X_train, y_train)

ovr_clf = OneVsRestClassifier(SVC())
ovr_clf.fit(X_train, y_train)

sgd_clf = SGDClassifier()
sgd_clf.fit(X_train, y_train)

print(cross_val_score(sgd_clf, X_train, y_train, cv=3, scoring="accuracy"))

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train.astype(np.float64))
print(cross_val_score(sgd_clf, X_train_scaled, y_train, cv=3, scoring="accuracy"))

# ERROR ANALYSIS

y_train_predict = cross_val_predict(sgd_clf, X_train_scaled, y_train, cv=3)
confusion_matrix = confusion_matrix(y_train, y_train_predict)
print(confusion_matrix)

plt.matshow(confusion_matrix, cmap=plt.cm.gray)
plt.show()

row_sums = confusion_matrix.sum(axis=1, keepdims=True)
normalized_confusion_matrix = confusion_matrix / row_sums
np.fill_diagonal(normalized_confusion_matrix, 0)

plt.matshow(normalized_confusion_matrix, cmap=plt.cm.gray)
plt.show()

cl_a, cl_b = 3, 5
X_aa = X_train[(y_train == cl_a) & (y_train_predict == cl_a)]
X_ab = X_train[(y_train == cl_a) & (y_train_predict == cl_b)]
X_ba = X_train[(y_train == cl_b) & (y_train_predict == cl_a)]
X_bb = X_train[(y_train == cl_b) & (y_train_predict == cl_b)]

plt.figure(figsize=(8, 8))
# TODO plot digits

# Multilabel Classification

y_train_large = (y_train >= 7)
y_train_odd = (y_train % 2 == 1)
y_multilabel = np.c_([y_train_large, y_train_odd])

knn_clf = KNeighborsClassifier()
knn_clf.fit(X_train, y_multilabel)

y_train_knn_predict = cross_val_predict(knn_clf, X_train, y_multilabel, cv=3)
print(f1_score(y_multilabel, y_train_knn_predict, average="macro"))

# Multioutput Classification

noise = np.random.randint(0, 100, (len(X_train), 784))
X_train_mod = X_train + noise
y_train_mod = X_train
noise = np.random.randint(0, 100, (len(X_test), 784))
X_test_mod = X_test + noise
y_test_mod = X_test

knn_clf = KNeighborsClassifier()
knn_clf.fit(X_train_mod, y_train_mod)
