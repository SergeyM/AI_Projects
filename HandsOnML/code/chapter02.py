import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from zlib import crc32
import hashlib
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
import utils
import matplotlib as mpl
import matplotlib.image as mpimg
from pandas.plotting import scatter_matrix
import joblib
from scipy import stats

from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OrdinalEncoder, OneHotEncoder
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV

mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)

DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"
HOUSING_PATH = os.path.join("..", "data", "housing")
HOUSING_FILE_NAME = "housing"

# GETTING THE DATA
utils.fetch_data(HOUSING_URL, HOUSING_PATH, HOUSING_FILE_NAME + ".tgz")
housing = utils.load_csv(os.path.join(HOUSING_PATH, HOUSING_FILE_NAME + ".csv"))

# UNDERSTANDING THE DATA
print(housing.head())
print(housing.info())
print(housing["ocean_proximity"].value_counts())
print(housing.describe())
housing.hist(bins=50, figsize=(20, 15))
plt.show()

# SPLIT TRAINING AND TEST SET

housing["income_category"] = pd.cut(housing["median_income"],
                                    bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
                                    labels=[1, 2, 3, 4, 5])
housing["income_category"].hist()
plt.show()

# Random Sampling
train_set, test_set = train_test_split(housing, test_size=0.2, random_state=42)

# Stratified Sampling
split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
train_index, test_index = next(split.split(housing, housing["income_category"]))
strat_train_set = housing.loc[train_index]
strat_test_set = housing.loc[test_index]

# Comparison
compare_props = pd.DataFrame({
    "Overall": housing["income_category"].value_counts() / len(housing),
    "Stratified": strat_test_set["income_category"].value_counts() / len(strat_test_set),
    "Random": test_set["income_category"].value_counts() / len(test_set)
}).sort_index()
compare_props["Rand. %error"] = 100 * compare_props["Random"] / compare_props["Overall"] - 100
compare_props["Strat. %error"] = 100 * compare_props["Stratified"] / compare_props["Overall"] - 100
print(compare_props)

for set_ in (strat_train_set, strat_test_set):
    set_.drop("income_category", axis=1, inplace=True)

# EXPLORE THE DATA

exploration_set = strat_train_set.copy()

california_img = mpimg.imread(os.path.join("../images/", "california.png"))
ax = exploration_set.plot(kind="scatter", x="longitude", y="latitude", figsize=(10, 7),
                          s=exploration_set['population'] / 100, label="Population",
                          c="median_house_value", cmap=plt.get_cmap("jet"),
                          colorbar=False, alpha=0.4)
plt.imshow(california_img, extent=[-124.55, -113.80, 32.45, 42.05], alpha=0.5,
           cmap=plt.get_cmap("jet"))
plt.ylabel("Latitude", fontsize=14)
plt.xlabel("Longitude", fontsize=14)

prices = exploration_set["median_house_value"]
tick_values = np.linspace(prices.min(), prices.max(), 11)
cbar = plt.colorbar(ticks=tick_values / prices.max())
cbar.ax.set_yticklabels(["$%dk" % (round(v / 1000)) for v in tick_values], fontsize=14)
cbar.set_label('Median House Value', fontsize=16)

plt.legend(fontsize=16)
plt.show()

# Looking for Correlations
# Correlation [-1, 1] => 1 means strong positive correlation, -1 means strong negative correlation
# Only measures linear correlation
correlation_matrix = exploration_set.corr()
print(correlation_matrix["median_house_value"].sort_values(ascending=False))

attributes = ["median_house_value", "median_income", "total_rooms", "housing_median_age"]
scatter_matrix(exploration_set[attributes], figsize=(12, 8))
plt.show()

exploration_set.plot(kind="scatter", x="median_income", y="median_house_value", alpha=0.1)
plt.show()

# Combining attributes and checking for correlation
exploration_set["rooms_per_household"] = exploration_set["total_rooms"] / exploration_set["households"]
exploration_set["bedrooms_per_room"] = exploration_set["total_bedrooms"] / exploration_set["total_rooms"]
exploration_set["population_per_household"] = exploration_set["population"] / exploration_set["households"]

correlation_matrix = exploration_set.corr()
print(correlation_matrix["median_house_value"].sort_values(ascending=False))

# PREPARE THE DATA

housing = strat_train_set.drop("median_house_value", axis=1)
housing_labels = strat_train_set["median_house_value"].copy()


# Data Cleaning

class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
    col_names = "total_rooms", "total_bedrooms", "population", "households"

    def __init__(self, data, add_bedrooms_per_room=True):  # no *args or **kwargs
        self.data = data
        self.room_ix, self.bedrooms_ix, self.population_ix, self.households_ix = [
            self.data.columns.get_loc(c) for c in self.col_names
        ]
        self.add_bedrooms_per_room = add_bedrooms_per_room

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        rooms_per_household = X[:, self.room_ix] / X[:, self.households_ix]
        population_per_household = X[:, self.population_ix] / X[:, self.households_ix]
        if self.add_bedrooms_per_room:
            bedrooms_per_room = X[:, self.bedrooms_ix] / X[:, self.room_ix]
            return np.c_[X, rooms_per_household, population_per_household, bedrooms_per_room]
        else:
            return np.c_[X, rooms_per_household, population_per_household]


housing_num = housing.drop("ocean_proximity", axis=1)
num_attribs = list(housing_num)
cat_attribs = ["ocean_proximity"]

num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="mean")),
    ('attribs_adder', CombinedAttributesAdder(housing_num)),
    ('std_scaler', StandardScaler())
])

full_pipeline = ColumnTransformer([
    ("num", num_pipeline, num_attribs),
    ("cat", OneHotEncoder(), cat_attribs)
])


housing_prepared = full_pipeline.fit_transform(housing)

extra_attibs = ["rooms_per_household", "population_per_household", "bedrooms_per_room"]
cat_one_hot_attribs = list(full_pipeline.named_transformers_["cat"].categories_[0])
attributes = num_attribs + extra_attibs + cat_one_hot_attribs
housing_prepared_df = pd.DataFrame(
    housing_prepared,
    columns=attributes,
    index=housing.index)

print(housing.head())
print(housing_prepared_df.head())


# SELECT AND TRAIN A MODEL

lin_reg = LinearRegression()
lin_reg.fit(housing_prepared, housing_labels)
housing_predictions = lin_reg.predict(housing_prepared)
print(mean_squared_error(housing_labels, housing_predictions, squared=False))

tree_reg = DecisionTreeRegressor()
tree_reg.fit(housing_prepared, housing_labels)
housing_predictions = tree_reg.predict(housing_prepared)
print(mean_squared_error(housing_labels, housing_predictions, squared=False))

lin_scores = cross_val_score(lin_reg, housing_prepared, housing_labels, scoring="neg_mean_squared_error", cv=10)
print(pd.Series(np.sqrt(-lin_scores)).describe())

tree_scores = cross_val_score(tree_reg, housing_prepared, housing_labels, scoring="neg_mean_squared_error", cv=10)
print(pd.Series(np.sqrt(-tree_scores)).describe())

forest_reg = RandomForestRegressor()
forest_reg.fit(housing_prepared, housing_labels)
housing_predictions = forest_reg.predict(housing_prepared)
print(mean_squared_error(housing_labels, housing_predictions, squared=False))

forest_scores = cross_val_score(forest_reg, housing_prepared, housing_labels, scoring="neg_mean_squared_error", cv=10)
print(pd.Series(np.sqrt(-forest_scores)).describe())

# Saving the Model
joblib.dump(forest_reg, "../models/housing_forest_reg.pkl")

# Loading the model
forest_reg = joblib.load("../models/housing_forest_reg.pkl")

# Hyperparameter Searching

param_grid = [
    {'n_estimators': [3, 10, 30], 'max_features': [2, 4, 6, 8]},
    {'bootstrap': [False], 'n_estimators': [3, 10], 'max_features': [2, 3, 4]}
]

forest_reg = RandomForestRegressor()

grid_search = GridSearchCV(forest_reg, param_grid, cv=5, scoring="neg_mean_squared_error", return_train_score=True)
grid_search.fit(housing_prepared, housing_labels)
print(grid_search.best_params_)

print(pd.DataFrame(grid_search.cv_results_))

feature_importances = grid_search.best_estimator_.feature_importances_
print(sorted(zip(feature_importances, attributes), reverse=True))

final_model = grid_search.best_estimator_

X_test = strat_test_set.drop("median_house_value", axis=1)
y_test = strat_test_set["median_house_value"].copy()

X_test_prepared = full_pipeline.transform(X_test)
final_predictions = final_model.predict(X_test_prepared)

final_mse = mean_squared_error(final_predictions, y_test)
final_rmse = np.sqrt(final_mse)

confidence = 0.95
squared_errors = (final_predictions - y_test) ** 2
print(np.sqrt(
    stats.t.interval(confidence, len(squared_errors) - 1, loc=squared_errors.mean(), scale=stats.sem(squared_errors))))

# full_pipeline_with_predictor = Pipeline([
#     ("preparation", full_pipeline),
#     ("linear", LinearRegression())
# ])
#
# full_pipeline_with_predictor.fit(housing, housing_labels)
# full_pipeline_with_predictor.predict(some_data)

"""
# Random Sampling
# Use Sklearn train_test_split() instead
def split_train_test(data, test_ratio):
    shuffled_indices = np.random.permutation(len(data))
    test_set_size = int(len(data) * test_ratio)
    test_indices = shuffled_indices[:test_set_size]
    train_indices = shuffled_indices[test_set_size:]
    return data.iloc[train_indices], data.iloc[test_indices]


def test_set_check(identifier, test_ratio):
    # use instance's identifier to decide whether or not it is in the test set
    # compute hash of identifier => put in test set if hash is lower than or equal to test_ratio of max hash value
    return crc32(np.int64(identifier)) & 0xffffffff < test_ratio * 2 ** 32

# def test_set_check(identifier, test_ratio, hash=hashlib.md5):
#     return hash(np.int64(identifier)).digest()[-1] < 256 * test_ratio
#
# def test_set_check(identifier, test_ratio, hash=hashlib.md5):
#     return bytearray(hash(np.int64(identifier)).digest())[-1] < 256 * test_ratio


def split_train_test_by_id(data, test_ratio, id_column):
    ids = data[id_column]
    in_test_set = ids.apply(lambda id_: test_set_check(id_, test_ratio))
    return data.loc[~in_test_set], data.loc[in_test_set]


housing_with_id = housing.reset_index()
train_set, test_set = split_train_test_by_id(housing_with_id, 0.2, "index")

----------------------

# Numerical Features

# housing.dropna(subset=["total_bedrooms"])       # option 1
# housing.drop("total_bedrooms", axis=1)          # option 2
# median = housing["total_bedrooms"].median()     # option 3 (save median for filling missing value in test set)
# housing["total_bedrooms"].fillna(median, inplace=True)

imputer = SimpleImputer(strategy="median")
imputer.fit(housing_num)  # calculates median of all numeric values and saves them
print(imputer.statistics_)
X = imputer.transform(housing_num)
print(pd.DataFrame(X, columns=housing_num.columns, index=housing_num.index))

# Categorical Features

# Ordinal Encoder (category to single number)
housing_cat = housing[["ocean_proximity"]]
cat_encoder = OrdinalEncoder()
housing_cat_encoded = cat_encoder.fit_transform(housing_cat)
print(cat_encoder.categories_)
print(housing_cat_encoded[:10])

# OneHotEncoder (category to one-hot vector)
cat_encoder = OneHotEncoder()
housing_cat_encoded = cat_encoder.fit_transform(housing_cat)
print(cat_encoder.categories_)
print(housing_cat_encoded.toarray())
"""
